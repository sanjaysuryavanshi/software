USE [ExT_Studio]
GO
/****** Object:  Table [dbo].[tbl_Admin_Login]    Script Date: 30/11/2017 20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Admin_Login](
	[Admin_Login_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Regi_Id] [bigint] NULL,
	[UserName] [nvarchar](50) NULL,
	[Email_Id] [nvarchar](150) NULL,
	[Password] [nvarchar](50) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Admin_Login] PRIMARY KEY CLUSTERED 
(
	[Admin_Login_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Banking_Booking_Mst]    Script Date: 30/11/2017 20:30 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Banking_Booking_Mst](
	[Banking_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Member_Id] [bigint] NULL,
	[Full_Name] [nvarchar](100) NULL,
	[Price] [numeric](18, 2) NULL,
	[IBAN] [nvarchar](150) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Banking_Booking_Mst] PRIMARY KEY CLUSTERED 
(
	[Banking_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Check_In_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Check_In_Mst](
	[CechkId] [bigint] IDENTITY(1,1) NOT NULL,
	[Probetraining_Id] [bigint] NULL,
	[Full_Name] [nvarchar](100) NULL,
	[BOD] [datetime] NULL,
	[Belt] [nvarchar](150) NULL,
	[NBTDate] [datetime] NULL,
	[NBTTime] [nvarchar](50) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Check_In_Mst] PRIMARY KEY CLUSTERED 
(
	[CechkId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Company_Detail_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Company_Detail_Mst](
	[Company_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Company_Name] [nvarchar](100) NULL,
	[Company_Short_Name] [nvarchar](10) NULL,
	[Company_Address_Line1] [nvarchar](100) NULL,
	[Company_Address_Line2] [nvarchar](100) NULL,
	[Company_Street] [nvarchar](50) NULL,
	[Company_Nearby] [nvarchar](50) NULL,
	[Company_Area] [nvarchar](50) NULL,
	[Company_City] [nvarchar](50) NULL,
	[Company_State] [nvarchar](50) NULL,
	[Company_Country] [nvarchar](50) NULL,
	[Company_Pincode] [numeric](18, 0) NULL,
	[Company_Email] [nvarchar](50) NULL,
	[Company_PhoneNo1] [numeric](18, 0) NULL,
	[Company_PhoneNo2] [numeric](18, 0) NULL,
	[Company_Website] [nvarchar](50) NULL,
	[Company_Logo] [nvarchar](500) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Company_Detail_Mst] PRIMARY KEY CLUSTERED 
(
	[Company_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Course_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Course_Mst](
	[Course_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Course_Name] [nvarchar](100) NULL,
	[Location_Id] [bigint] NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Course_Mst] PRIMARY KEY CLUSTERED 
(
	[Course_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_EmailConfigration_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_EmailConfigration_Mst](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[Host] [nvarchar](max) NULL,
	[Port] [numeric](18, 0) NULL,
	[UserName] [nvarchar](max) NULL,
	[Password] [nvarchar](max) NULL,
	[SSL] [bit] NULL,
	[EmailType] [nvarchar](max) NULL,
 CONSTRAINT [PK_tbl_EmailConfigration_Mst] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Exception]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Exception](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ErrorStatusCode] [nvarchar](max) NULL,
	[ExcType] [nvarchar](max) NULL,
	[ExceptionMsg] [nvarchar](max) NULL,
	[ExTrace] [nvarchar](max) NULL,
	[ExSource] [nvarchar](max) NULL,
	[WebURL] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_Exception] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Location_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Location_Mst](
	[Location_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[T_Day] [nvarchar](100) NULL,
	[Is_Holiday] [nchar](1) NULL,
	[Holiday_Date] [datetime] NULL,
	[Location_Name] [nvarchar](100) NULL,
	[Address] [nvarchar](max) NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Location_Mst] PRIMARY KEY CLUSTERED 
(
	[Location_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Membership_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Membership_Mst](
	[Member_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[TTime_Id] [bigint] NULL,
	[Trainer_Id] [bigint] NULL,
	[Student_Name] [nvarchar](100) NULL,
	[Address] [nvarchar](max) NULL,
	[Zip_Code] [numeric](18, 0) NULL,
	[Country] [nvarchar](100) NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[Email_Id] [nvarchar](150) NULL,
	[IBAN] [nvarchar](150) NULL,
	[M_Option_Id] [bigint] NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Membership_Mst] PRIMARY KEY CLUSTERED 
(
	[Member_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Membership_Options_Charges]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Membership_Options_Charges](
	[M_Option_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Is_Kide_Adult] [nchar](1) NULL,
	[Starter_Price] [numeric](18, 2) NULL,
	[Package_Name] [nvarchar](50) NULL,
	[Package_Time] [nvarchar](50) NULL,
	[Times_Per_Week] [nvarchar](50) NULL,
	[Monthly_Investment] [numeric](18, 2) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Membership_Options_Charges] PRIMARY KEY CLUSTERED 
(
	[M_Option_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Membership_Product_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Membership_Product_Mst](
	[M_Product_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[M_Option_Id] [bigint] NULL,
	[Product_Name] [nvarchar](100) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Membership_Product_Mst] PRIMARY KEY CLUSTERED 
(
	[M_Product_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Message_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Message_Mst](
	[Message_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[BVK] [nvarchar](max) NULL,
	[Users] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Message_Mst] PRIMARY KEY CLUSTERED 
(
	[Message_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Monthly_Bookings_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Monthly_Bookings_Mst](
	[M_Booking_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Booking_Date] [datetime] NULL,
	[Probetraining_Id] [bigint] NULL,
	[Joining_Fee] [numeric](18, 2) NULL,
	[Monthly_Booking] [numeric](18, 2) NULL,
	[IBAN] [nvarchar](50) NULL,
	[Total] [numeric](18, 2) NULL,
	[IsPaid] [nchar](1) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Monthly_Bookings_Mst] PRIMARY KEY CLUSTERED 
(
	[M_Booking_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Open_Bookings_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Open_Bookings_Mst](
	[O_Booking_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Booking_Date] [datetime] NULL,
	[Probetraining_Id] [bigint] NULL,
	[Joining_Fee] [numeric](18, 2) NULL,
	[Monthly_Booking] [numeric](18, 2) NULL,
	[IBAN] [nvarchar](50) NULL,
	[Total] [numeric](18, 2) NULL,
	[IsPaid] [nchar](1) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Open_Bookings_Mst] PRIMARY KEY CLUSTERED 
(
	[O_Booking_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Probetraining_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Probetraining_Mst](
	[Probetraining_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Location_Id] [bigint] NULL,
	[Trainer_Id] [bigint] NULL,
	[Full_Name] [nvarchar](100) NULL,
	[Email_Id] [nvarchar](150) NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[Age] [bigint] NULL,
	[Heard] [nvarchar](50) NULL,
	[GoogleFacebook] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Probetraining_Mst] PRIMARY KEY CLUSTERED 
(
	[Probetraining_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Product_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Product_Mst](
	[Product_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Product_Name] [nvarchar](100) NULL,
	[Size] [nvarchar](50) NULL,
	[Price] [numeric](18, 0) NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Product_Mst] PRIMARY KEY CLUSTERED 
(
	[Product_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Registration]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Registration](
	[Regi_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Full_Name] [nvarchar](100) NULL,
	[Shcool_Name] [nvarchar](100) NULL,
	[Address] [nvarchar](max) NULL,
	[Zip_Code] [numeric](18, 0) NULL,
	[Country] [nvarchar](100) NULL,
	[Email_Id] [nvarchar](150) NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Registration] PRIMARY KEY CLUSTERED 
(
	[Regi_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Sms_Api_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Sms_Api_Mst](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[ApiURL] [nvarchar](max) NULL,
	[UserName] [nvarchar](100) NULL,
	[Password] [nvarchar](100) NULL,
	[SenderID] [nvarchar](100) NULL,
	[Priorty] [nvarchar](100) NULL,
	[Type] [nvarchar](100) NULL,
	[ApiFlag] [nvarchar](50) NULL,
 CONSTRAINT [PK_tbl_SmsApi_Mst] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Trainer_mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Trainer_mst](
	[Trainer_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ImagePath] [nvarchar](max) NULL,
	[Full_Name] [nvarchar](100) NULL,
	[Email_Id] [nvarchar](150) NULL,
	[See] [nvarchar](50) NULL,
	[Do] [nvarchar](50) NULL,
	[Admin_Id] [bigint] NULL,
	[MobileNo] [numeric](18, 0) NULL,
	[Password] [nvarchar](50) NULL,
	[About] [nvarchar](max) NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Trainer_mst] PRIMARY KEY CLUSTERED 
(
	[Trainer_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tbl_Training_Timings]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tbl_Training_Timings](
	[TTime_Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Location_Id] [bigint] NULL,
	[T_Day] [nvarchar](100) NULL,
	[Is_Holiday] [nchar](1) NULL,
	[Holiday_Date] [datetime] NULL,
	[Course_Id] [bigint] NULL,
	[Course_Form] [nvarchar](50) NULL,
	[Course_To] [nvarchar](50) NULL,
	[AddOnce] [nvarchar](50) NULL,
	[EveryWeek] [nvarchar](50) NULL,
	[Trainer_Id] [bigint] NULL,
	[Flag] [nchar](1) NULL,
	[CreateDate] [datetime] NULL,
	[UpdateDate] [datetime] NULL,
	[CreateUser] [bigint] NULL,
	[UpdateUser] [bigint] NULL,
 CONSTRAINT [PK_tbl_Training_Timings] PRIMARY KEY CLUSTERED 
(
	[TTime_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[tbl_Admin_Login] ON 

INSERT [dbo].[tbl_Admin_Login] ([Admin_Login_Id], [Regi_Id], [UserName], [Email_Id], [Password], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 1, N'Admin', N'admin@gmail.com', N'123', N'A', CAST(N'2017-11-16T17:00:35.073' AS DateTime), NULL, 0, 1)
SET IDENTITY_INSERT [dbo].[tbl_Admin_Login] OFF
SET IDENTITY_INSERT [dbo].[tbl_Banking_Booking_Mst] ON 

INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 1, NULL, NULL, N'fghffgh', N'A', CAST(N'2017-11-21T16:51:13.817' AS DateTime), CAST(N'2017-11-30T14:34:53.380' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, 2, NULL, NULL, N'456456', N'A', CAST(N'2017-11-21T16:55:11.473' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, 3, NULL, NULL, N'dfgdfg', N'A', CAST(N'2017-11-21T17:02:57.720' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, 4, NULL, NULL, N'1232136', N'A', CAST(N'2017-11-21T17:13:00.433' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, 5, NULL, NULL, N'assd', N'A', CAST(N'2017-11-23T18:36:48.617' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (6, 1, NULL, NULL, N'sdf', N'A', CAST(N'2017-11-24T11:00:29.970' AS DateTime), CAST(N'2017-11-30T14:34:53.380' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (7, 2, NULL, NULL, N'54', N'A', CAST(N'2017-11-24T11:59:02.770' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (8, 3, NULL, NULL, N'asd', N'A', CAST(N'2017-11-24T12:10:54.527' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (9, 4, NULL, NULL, N'6646', N'A', CAST(N'2017-11-25T11:20:36.090' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (10, 5, NULL, NULL, N'dgs', N'A', CAST(N'2017-11-25T12:24:26.633' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (11, 6, NULL, NULL, N'werwerw', N'A', CAST(N'2017-11-27T12:06:20.233' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (12, 7, NULL, NULL, N'dfgdf', N'A', CAST(N'2017-11-27T18:04:23.100' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (13, 8, NULL, NULL, N'8796', N'A', CAST(N'2017-11-30T11:20:41.307' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (14, 9, NULL, NULL, N'213', N'A', CAST(N'2017-11-30T12:05:08.360' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (15, 10, NULL, NULL, N'213', N'A', CAST(N'2017-11-30T12:07:55.987' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Banking_Booking_Mst] ([Banking_Id], [Member_Id], [Full_Name], [Price], [IBAN], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (16, 11, NULL, NULL, N'ghsgf', N'A', CAST(N'2017-11-30T13:23:33.787' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Banking_Booking_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Check_In_Mst] ON 

INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 2, N'Rajesh', CAST(N'2000-10-02T00:00:00.000' AS DateTime), N'yellow', CAST(N'2017-11-26T00:00:00.000' AS DateTime), N'10:00', N'A', CAST(N'2017-11-27T11:34:11.283' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, 6, N'samir', CAST(N'2017-11-02T00:00:00.000' AS DateTime), N'yellow', CAST(N'2017-11-30T00:00:00.000' AS DateTime), N'10:00', N'A', CAST(N'2017-11-27T17:59:39.757' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, 6, N'samir', CAST(N'2017-11-02T00:00:00.000' AS DateTime), N'yellow', CAST(N'2017-11-30T00:00:00.000' AS DateTime), N'10:00', N'A', CAST(N'2017-11-27T18:00:50.503' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, 7, N'Montu Patel', CAST(N'2017-11-19T00:00:00.000' AS DateTime), N'yellow', CAST(N'2017-11-25T00:00:00.000' AS DateTime), N'00:10', N'A', CAST(N'2017-11-29T16:53:55.113' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, 7, N'Montu Patel', CAST(N'2017-11-19T00:00:00.000' AS DateTime), N'Red', CAST(N'2017-11-30T00:00:00.000' AS DateTime), N'01:10', N'A', CAST(N'2017-11-29T16:54:13.643' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (6, 7, N'Montu Patel', CAST(N'2017-11-19T00:00:00.000' AS DateTime), N'Red', CAST(N'2017-11-30T00:00:00.000' AS DateTime), N'01:10', N'A', CAST(N'2017-11-29T16:54:17.480' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Check_In_Mst] ([CechkId], [Probetraining_Id], [Full_Name], [BOD], [Belt], [NBTDate], [NBTTime], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (7, 1, N'Manoj', CAST(N'2000-06-10T00:00:00.000' AS DateTime), N'Yellow', CAST(N'2017-12-20T00:00:00.000' AS DateTime), N'10:00', N'A', CAST(N'2017-11-30T11:59:25.727' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Check_In_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Company_Detail_Mst] ON 

INSERT [dbo].[tbl_Company_Detail_Mst] ([Company_Id], [Company_Name], [Company_Short_Name], [Company_Address_Line1], [Company_Address_Line2], [Company_Street], [Company_Nearby], [Company_Area], [Company_City], [Company_State], [Company_Country], [Company_Pincode], [Company_Email], [Company_PhoneNo1], [Company_PhoneNo2], [Company_Website], [Company_Logo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'Studio', N'st', N'asd', N'asd', N'asd', N'asd', N'asd', N'Ahmedabad', N'Gujarat', N'India', CAST(123456 AS Numeric(18, 0)), N'studio@gmail.com', CAST(298765432 AS Numeric(18, 0)), CAST(7985645645 AS Numeric(18, 0)), N'www.studio.com', N'../images/no-image.png', N'A', CAST(N'2017-11-24T13:04:01.667' AS DateTime), CAST(N'2017-11-24T13:04:13.003' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Company_Detail_Mst] ([Company_Id], [Company_Name], [Company_Short_Name], [Company_Address_Line1], [Company_Address_Line2], [Company_Street], [Company_Nearby], [Company_Area], [Company_City], [Company_State], [Company_Country], [Company_Pincode], [Company_Email], [Company_PhoneNo1], [Company_PhoneNo2], [Company_Website], [Company_Logo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'sdf', N'asdf', N'asfd', N'asdf', N'asfd', N'asfd', N'sadf', N'asdf', N'asdf', N'asdf', CAST(987654 AS Numeric(18, 0)), N'sdaffa@ad.asd', CAST(789654321 AS Numeric(18, 0)), CAST(213465879 AS Numeric(18, 0)), N'dfgsdgf', N'../Images/Company/sdf789654321_Company.jpg', N'A', CAST(N'2017-11-30T12:02:27.720' AS DateTime), CAST(N'2017-11-30T12:03:18.360' AS DateTime), 1, 0)
SET IDENTITY_INSERT [dbo].[tbl_Company_Detail_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Course_Mst] ON 

INSERT [dbo].[tbl_Course_Mst] ([Course_Id], [Course_Name], [Location_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'Kinder Karate', 1, N'A', CAST(N'2017-11-17T16:09:55.180' AS DateTime), CAST(N'2017-11-17T16:12:42.583' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Course_Mst] ([Course_Id], [Course_Name], [Location_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'Self defence', 3, N'A', CAST(N'2017-11-17T16:13:00.577' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Course_Mst] ([Course_Id], [Course_Name], [Location_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'XYZ', 2, N'D', CAST(N'2017-11-29T15:42:39.180' AS DateTime), CAST(N'2017-11-29T15:59:38.233' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[tbl_Course_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_EmailConfigration_Mst] ON 

INSERT [dbo].[tbl_EmailConfigration_Mst] ([ID], [Host], [Port], [UserName], [Password], [SSL], [EmailType]) VALUES (1, N'smtp.gmail.com', CAST(587 AS Numeric(18, 0)), N'excelsiordevlopers@gmail.com', N'ExT@Excelsior@Team', 1, N'NoReplay')
SET IDENTITY_INSERT [dbo].[tbl_EmailConfigration_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Exception] ON 

INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (1, N'1000', N'System.Data.SqlClient.SqlException', N'Parameter ''@Password'' was supplied multiple times.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.InternalExecuteNonQuery(TaskCompletionSource`1 completion, String methodName, Boolean sendToPipe, Int32 timeout, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.ExecuteNonQuery()
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl)
   at DAL.InteractionMethod.Registration_Insert(Prop objValue)
   at ExTStudio.Admin.create_new_account.lbtnSend_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\create-new-account.aspx.cs:line 89', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/create-new-account.aspx', N'A', CAST(N'2017-11-16T17:00:29.113' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (2, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.location.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\location.aspx.cs:line 92', N'mscorlib', N'http://localhost:58352/Admin/location.aspx', N'A', CAST(N'2017-11-16T20:02:44.360' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (3, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.location.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\location.aspx.cs:line 92', N'mscorlib', N'http://localhost:58352/Admin/location.aspx', N'A', CAST(N'2017-11-17T10:33:01.617' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (4, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.location.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\location.aspx.cs:line 92', N'mscorlib', N'http://localhost:58352/Admin/location.aspx', N'A', CAST(N'2017-11-17T10:34:34.967' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (5, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.trainers.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\trainers.aspx.cs:line 96', N'mscorlib', N'http://localhost:58352/Admin/trainers.aspx', N'A', CAST(N'2017-11-17T15:20:45.600' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10005, N'1000', N'System.ArgumentException', N'Column ''Location_Id'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.check_in2.repCourse_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\course.aspx.cs:line 253', N'System.Data', N'http://localhost:58352/Admin/course.aspx', N'A', CAST(N'2017-11-17T16:10:12.723' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10006, N'1000', N'System.Data.SqlClient.SqlException', N'@Is_Kide_Adult is not a parameter for procedure SP_Training_Timings.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.InternalExecuteNonQuery(TaskCompletionSource`1 completion, String methodName, Boolean sendToPipe, Int32 timeout, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.ExecuteNonQuery()
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 92
   at DAL.InteractionMethod.Membership_Options_Insert(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 563
   at ExTStudio.Admin.membership_options_kids.lbtnNext_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-kids.aspx.cs:line 70', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/membership-options-kids.aspx', N'A', CAST(N'2017-11-17T18:36:22.573' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10007, N'1000', N'System.Data.SqlClient.SqlException', N'Error converting data type nvarchar to numeric.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.InternalExecuteNonQuery(TaskCompletionSource`1 completion, String methodName, Boolean sendToPipe, Int32 timeout, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.ExecuteNonQuery()
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 92
   at DAL.InteractionMethod.Membership_Options_Insert(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 563
   at ExTStudio.Admin.membership_options_kids.lbtnNext_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-kids.aspx.cs:line 70', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/membership-options-kids.aspx', N'A', CAST(N'2017-11-17T18:38:14.410' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10008, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseDecimal(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToDecimal(String value)
   at ExTStudio.Admin.monthly_bookings.txtMonthlyBookings_TextChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\monthly-bookings.aspx.cs:line 137', N'mscorlib', N'http://localhost:58352/Admin/monthly-bookings.aspx', N'A', CAST(N'2017-11-18T15:23:56.263' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10009, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseDecimal(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToDecimal(String value)
   at ExTStudio.Admin.monthly_bookings.txtMonthlyBookings_TextChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\monthly-bookings.aspx.cs:line 137', N'mscorlib', N'http://localhost:58352/Admin/monthly-bookings.aspx', N'A', CAST(N'2017-11-18T15:24:06.320' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10010, N'1000', N'System.Web.HttpException', N'DataBinding: ''System.Data.DataRowView'' does not contain a property with the name ''Price''.', N'   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName)
   at System.Web.UI.DataBinder.Eval(Object container, String[] expressionParts)
   at System.Web.UI.DataBinder.Eval(Object container, String expression)
   at System.Web.UI.TemplateControl.Eval(String expression)
   at ASP.admin_monthly_bookings_aspx.__DataBind__control30(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\monthly-bookings.aspx:line 216
   at System.Web.UI.Control.OnDataBinding(EventArgs e)
   at System.Web.UI.Control.DataBind(Boolean raiseOnDataBinding)
   at System.Web.UI.Control.DataBind()
   at System.Web.UI.Control.DataBindChildren()
   at System.Web.UI.Control.DataBind(Boolean raiseOnDataBinding)
   at System.Web.UI.Control.DataBind()
   at System.Web.UI.WebControls.ListView.CreateItemsWithoutGroups(ListViewPagedDataSource dataSource, Boolean dataBinding, InsertItemPosition insertPosition, ArrayList keyArray)
   at System.Web.UI.WebControls.ListView.CreateChildControls(IEnumerable dataSource, Boolean dataBinding)
   at System.Web.UI.WebControls.ListView.PerformDataBinding(IEnumerable data)
   at System.Web.UI.WebControls.DataBoundControl.OnDataSourceViewSelectCallback(IEnumerable data)
   at System.Web.UI.DataSourceView.Select(DataSourceSelectArguments arguments, DataSourceViewSelectCallback callback)
   at System.Web.UI.WebControls.DataBoundControl.PerformSelect()
   at System.Web.UI.WebControls.ListView.PerformSelect()
   at System.Web.UI.WebControls.BaseDataBoundControl.DataBind()
   at ExTStudio.Admin.monthly_bookings.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\monthly-bookings.aspx.cs:line 98', N'System.Web', N'http://localhost:58352/Admin/monthly-bookings.aspx', N'A', CAST(N'2017-11-18T15:35:05.257' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10011, N'1000', N'System.IndexOutOfRangeException', N'Cannot find table 3.', N'   at System.Data.DataTableCollection.get_Item(Int32 index)
   at ExTStudio.Admin.monthly_bookings.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\monthly-bookings.aspx.cs:line 106', N'System.Data', N'http://localhost:58352/Admin/monthly-bookings.aspx', N'A', CAST(N'2017-11-18T16:03:56.857' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10012, N'1000', N'System.Data.SqlClient.SqlException', N'Could not find stored procedure ''SP_Open_Bookings_Mst''.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.System.Data.IDbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Open_Bookings_SelectAll() in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 783
   at ExTStudio.Admin.open_booking.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 91', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:16:19.077' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10013, N'1000', N'System.Data.SqlClient.SqlException', N'Could not find stored procedure ''SP_Open_Bookings_Mst''.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.InternalExecuteNonQuery(TaskCompletionSource`1 completion, String methodName, Boolean sendToPipe, Int32 timeout, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.ExecuteNonQuery()
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 92
   at DAL.InteractionMethod.Open_Bookings_Insert(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 809
   at ExTStudio.Admin.open_booking.lbtnBookNow_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 140', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:16:29.933' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10014, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.open_booking.BindStudent() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 65', N'ExTStudio', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:18:01.793' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10015, N'1000', N'System.Data.SqlClient.SqlException', N'Could not find stored procedure ''SP_Open_Bookings_Mst''.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.System.Data.IDbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Open_Bookings_SelectAll() in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 783
   at ExTStudio.Admin.open_booking.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 91', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:18:01.867' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10016, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.open_booking.BindStudent() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 65', N'ExTStudio', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:18:26.333' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10017, N'1000', N'System.Data.SqlClient.SqlException', N'Could not find stored procedure ''SP_Open_Bookings_Mst''.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryConsumeMetaData()
   at System.Data.SqlClient.SqlDataReader.get_MetaData()
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteReader(CommandBehavior behavior, String method)
   at System.Data.SqlClient.SqlCommand.ExecuteDbDataReader(CommandBehavior behavior)
   at System.Data.Common.DbCommand.System.Data.IDbCommand.ExecuteReader(CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Open_Bookings_SelectAll() in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 783
   at ExTStudio.Admin.open_booking.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 91', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:18:26.360' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10018, N'1000', N'System.Web.HttpException', N'DataBinding: ''System.Data.DataRowView'' does not contain a property with the name ''O_Booking_Id''.', N'   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName)
   at System.Web.UI.DataBinder.Eval(Object container, String[] expressionParts)
   at System.Web.UI.DataBinder.Eval(Object container, String expression)
   at System.Web.UI.TemplateControl.Eval(String expression)
   at ASP.admin_open_booking_aspx.__DataBind__control12(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx:line 81
   at System.Web.UI.Control.OnDataBinding(EventArgs e)
   at System.Web.UI.Control.DataBind(Boolean raiseOnDataBinding)
   at System.Web.UI.Control.DataBind()
   at System.Web.UI.Control.DataBindChildren()
   at System.Web.UI.Control.DataBind(Boolean raiseOnDataBinding)
   at System.Web.UI.Control.DataBind()
   at System.Web.UI.WebControls.ListView.CreateItemsWithoutGroups(ListViewPagedDataSource dataSource, Boolean dataBinding, InsertItemPosition insertPosition, ArrayList keyArray)
   at System.Web.UI.WebControls.ListView.CreateChildControls(IEnumerable dataSource, Boolean dataBinding)
   at System.Web.UI.WebControls.ListView.PerformDataBinding(IEnumerable data)
   at System.Web.UI.WebControls.DataBoundControl.OnDataSourceViewSelectCallback(IEnumerable data)
   at System.Web.UI.DataSourceView.Select(DataSourceSelectArguments arguments, DataSourceViewSelectCallback callback)
   at System.Web.UI.WebControls.DataBoundControl.PerformSelect()
   at System.Web.UI.WebControls.ListView.PerformSelect()
   at System.Web.UI.WebControls.BaseDataBoundControl.DataBind()
   at ExTStudio.Admin.open_booking.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\open-booking.aspx.cs:line 67', N'System.Web', N'http://localhost:58352/Admin/open-booking.aspx', N'A', CAST(N'2017-11-18T16:19:50.730' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10019, N'1000', N'System.Data.SqlClient.SqlException', N'Could not find stored procedure ''SP_Open_Bookings_Mst''.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlCommand.FinishExecuteReader(SqlDataReader ds, RunBehavior runBehavior, String resetOptionsString, Boolean isInternal, Boolean forDescribeParameterEncryption)
   at System.Data.SqlClient.SqlCommand.RunExecuteReaderTds(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, Boolean async, Int32 timeout, Task& task, Boolean asyncWrite, Boolean inRetry, SqlDataReader ds, Boolean describeParameterEncryptionRequest)
   at System.Data.SqlClient.SqlCommand.RunExecuteReader(CommandBehavior cmdBehavior, RunBehavior runBehavior, Boolean returnStream, String method, TaskCompletionSource`1 completion, Int32 timeout, Task& task, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.InternalExecuteNonQuery(TaskCompletionSource`1 completion, String methodName, Boolean sendToPipe, Int32 timeout, Boolean& usedCache, Boolean asyncWrite, Boolean inRetry)
   at System.Data.SqlClient.SqlCommand.ExecuteNonQuery()
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 92
   at DAL.InteractionMethod.Open_Bookings_Delete(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 837
   at ExTStudio.Admin.reminder_message.repBooking_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 93', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/reminder-message.aspx', N'A', CAST(N'2017-11-18T16:28:27.970' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10020, N'1000', N'System.IndexOutOfRangeException', N'Cannot find table 3.', N'   at System.Data.DataTableCollection.get_Item(Int32 index)
   at ExTStudio.Admin.membership.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 63', N'System.Data', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-18T18:27:41.193' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10021, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_product_add.BindData()', N'mscorlib', N'http://localhost:58352/Admin/membership-product-add.aspx?MOptionId=System.String[]', N'A', CAST(N'2017-11-21T12:53:25.777' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10022, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership_options_kids_list.repOption_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-kids-list.aspx.cs:line 138', N'mscorlib', N'http://localhost:58352/Admin/membership-options-kids-list.aspx', N'A', CAST(N'2017-11-21T13:35:05.297' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10023, N'1000', N'System.InvalidCastException', N'Unable to cast object of type ''System.Web.UI.WebControls.HiddenField'' to type ''System.IConvertible''.', N'   at System.Convert.ToInt64(Object value)
   at ExTStudio.Admin.membership_product_add.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-product-add.aspx.cs:line 171', N'mscorlib', N'http://localhost:58352/Admin/membership-product-add.aspx?MOptionId=7', N'A', CAST(N'2017-11-21T14:27:55.993' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10024, N'1000', N'System.ArgumentNullException', N'The SqlParameterCollection only accepts non-null SqlParameter type objects.
Parameter name: value', N'   at System.Data.SqlClient.SqlParameterCollection.ValidateType(Object value)
   at System.Data.SqlClient.SqlParameterCollection.Add(Object value)
   at System.Data.SqlClient.SqlParameterCollection.Add(SqlParameter value)
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 82
   at DAL.InteractionMethod.Membership_Product_Update(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 1038
   at ExTStudio.Admin.membership_product_add.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-product-add.aspx.cs:line 173', N'System.Data', N'http://localhost:58352/Admin/membership-product-add.aspx?MOptionId=6', N'A', CAST(N'2017-11-21T14:29:24.273' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10025, N'1000', N'System.ArgumentNullException', N'The SqlParameterCollection only accepts non-null SqlParameter type objects.
Parameter name: value', N'   at System.Data.SqlClient.SqlParameterCollection.ValidateType(Object value)
   at System.Data.SqlClient.SqlParameterCollection.Add(Object value)
   at System.Data.SqlClient.SqlParameterCollection.Add(SqlParameter value)
   at DAL.DBInteraction.ExecQryPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 82
   at DAL.InteractionMethod.Membership_Product_Update(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 1038
   at ExTStudio.Admin.membership_product_add.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-product-add.aspx.cs:line 173', N'System.Data', N'http://localhost:58352/Admin/membership-product-add.aspx?MOptionId=6', N'A', CAST(N'2017-11-21T14:30:20.237' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10026, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership_options_adult_list.repOption_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-adult-list.aspx.cs:line 139', N'mscorlib', N'http://localhost:58352/Admin/membership-options-adult-list.aspx', N'A', CAST(N'2017-11-21T14:47:45.057' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10027, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership_options_adult_list.repOption_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-adult-list.aspx.cs:line 139', N'mscorlib', N'http://localhost:58352/Admin/membership-options-adult-list.aspx', N'A', CAST(N'2017-11-21T14:50:12.657' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10028, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership_options_adult_list.repOption_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-adult-list.aspx.cs:line 139', N'mscorlib', N'http://localhost:58352/Admin/membership-options-adult-list.aspx', N'A', CAST(N'2017-11-21T14:50:22.760' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10029, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership_options_adult_list.repOption_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-options-adult-list.aspx.cs:line 139', N'mscorlib', N'http://localhost:58352/Admin/membership-options-adult-list.aspx', N'A', CAST(N'2017-11-21T14:51:21.793' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10030, N'1000', N'System.FormatException', N'String was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(String s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at ExTStudio.Admin.probetraining.ddlLocation_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\probetraining.aspx.cs:line 163', N'mscorlib', N'http://localhost:58352/Admin/probetraining.aspx', N'A', CAST(N'2017-11-21T15:47:54.477' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10031, N'1000', N'System.FormatException', N'String was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(String s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at ExTStudio.Admin.probetraining.ddlLocation_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\probetraining.aspx.cs:line 163', N'mscorlib', N'http://localhost:58352/Admin/probetraining.aspx', N'A', CAST(N'2017-11-21T15:47:58.517' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10032, N'1000', N'System.FormatException', N'String was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(String s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at ExTStudio.Admin.probetraining.ddlLocation_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\probetraining.aspx.cs:line 163', N'mscorlib', N'http://localhost:58352/Admin/probetraining.aspx', N'A', CAST(N'2017-11-21T15:50:37.580' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10033, N'1000', N'System.FormatException', N'String was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(String s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at ExTStudio.Admin.probetraining.ddlLocation_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\probetraining.aspx.cs:line 163', N'mscorlib', N'http://localhost:58352/Admin/probetraining.aspx', N'A', CAST(N'2017-11-21T15:50:40.120' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10034, N'1000', N'System.Web.HttpException', N'DataBinding: ''System.Data.DataRowView'' does not contain a property with the name ''Location_Name''.', N'   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName)
   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName, String format)
   at System.Web.UI.WebControls.ListControl.PerformDataBinding(IEnumerable dataSource)
   at System.Web.UI.WebControls.ListControl.OnDataBinding(EventArgs e)
   at System.Web.UI.WebControls.ListControl.PerformSelect()
   at System.Web.UI.WebControls.BaseDataBoundControl.DataBind()
   at ExTStudio.Admin.membership_option_select.rblKA_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 127', N'System.Web', N'http://localhost:58352/Admin/membership-option-select.aspx', N'A', CAST(N'2017-11-21T16:29:34.770' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10035, N'1000', N'System.Web.HttpException', N'DataBinding: ''System.Data.DataRowView'' does not contain a property with the name ''Location_Name''.', N'   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName)
   at System.Web.UI.DataBinder.GetPropertyValue(Object container, String propName, String format)
   at System.Web.UI.WebControls.ListControl.PerformDataBinding(IEnumerable dataSource)
   at System.Web.UI.WebControls.ListControl.OnDataBinding(EventArgs e)
   at System.Web.UI.WebControls.ListControl.PerformSelect()
   at System.Web.UI.WebControls.BaseDataBoundControl.DataBind()
   at ExTStudio.Admin.membership_option_select.rblKA_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 127', N'System.Web', N'http://localhost:58352/Admin/membership-option-select.aspx', N'A', CAST(N'2017-11-21T16:29:36.997' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10036, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.ddlMOption_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 148', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=2', N'A', CAST(N'2017-11-21T17:00:20.803' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10037, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.ddlMOption_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 155', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=3', N'A', CAST(N'2017-11-21T17:03:10.270' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10038, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.ddlMOption_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 156', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=1', N'A', CAST(N'2017-11-24T11:00:39.463' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10039, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.ddlMOption_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 156', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=1', N'A', CAST(N'2017-11-24T11:00:45.040' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10040, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.training_timings.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\training-timings.aspx.cs:line 65', N'ExTStudio', N'http://localhost:58352/Admin/training-timings.aspx', N'A', CAST(N'2017-11-24T14:50:58.417' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10041, N'1000', N'System.ArgumentException', N'Column ''Regi_Id'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.login.lbtnLogIn_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\login.aspx.cs:line 48', N'System.Data', N'http://localhost:58352/Admin/login.aspx', N'A', CAST(N'2017-11-25T10:55:21.617' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10042, N'1000', N'System.ArgumentException', N'Column ''Regi_Id'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.login.lbtnLogIn_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\login.aspx.cs:line 48', N'System.Data', N'http://localhost:58352/Admin/login.aspx', N'A', CAST(N'2017-11-25T10:55:26.023' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10043, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin._default.Page_Load(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\default.aspx.cs:line 35', N'ExTStudio', N'http://localhost:58352/Admin/default.aspx', N'A', CAST(N'2017-11-25T11:19:44.983' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10044, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.lbtnSave_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 73', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=4', N'A', CAST(N'2017-11-25T11:20:42.343' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10045, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.membership_option_select.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 191', N'ExTStudio', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=5', N'A', CAST(N'2017-11-25T12:37:37.457' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10046, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at CrystalDecisions.Shared.SharedUtils.WriteToResponse(HttpResponse response, Stream inputStream, Boolean exclusive)
   at CrystalDecisions.CrystalReports.Engine.ReportDocument.ExportToHttpResponse(ExportOptions options, HttpResponse response, Boolean asAttachment, String attachmentName)
   at CrystalDecisions.CrystalReports.Engine.ReportDocument.ExportToHttpResponse(ExportFormatType formatType, HttpResponse response, Boolean asAttachment, String attachmentName)
   at ExTStudio.Admin.membership_option_select.lbtnDownload_Click(Object sender, EventArgs e)', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=5', N'A', CAST(N'2017-11-25T12:43:16.943' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10047, N'1000', N'System.IndexOutOfRangeException', N'Cannot find table 0.', N'   at System.Data.DataTableCollection.get_Item(Int32 index)
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 98', N'System.Data', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T13:10:51.103' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10048, N'1000', N'System.IO.DirectoryNotFoundException', N'Could not find a part of the path ''E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\PDFs\MyPDF\20171125143748721.pdf''.', N'   at System.IO.__Error.WinIOError(Int32 errorCode, String maybeFullPath)
   at System.IO.FileStream.Init(String path, FileMode mode, FileAccess access, Int32 rights, Boolean useRights, FileShare share, Int32 bufferSize, FileOptions options, SECURITY_ATTRIBUTES secAttrs, String msgPath, Boolean bFromProxy, Boolean useLongPath, Boolean checkHost)
   at System.IO.FileStream..ctor(String path, FileMode mode, FileAccess access)
   at PdfSharp.Pdf.PdfDocument.Save(String path)
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 123', N'mscorlib', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T14:37:48.743' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10049, N'1000', N'System.Net.WebException', N'Could not find file ''C:\Program Files (x86)\IIS Express\MyPDF20171125144615664.pdf''.', N'   at System.Net.WebClient.DownloadFile(Uri address, String fileName)
   at System.Net.WebClient.DownloadFile(String address, String fileName)
   at ExTStudio.AllClasses.ComanClass.SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\AllClasses\ComanClass.cs:line 109
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 128', N'System', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T14:47:08.763' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10050, N'1000', N'System.Net.WebException', N'Could not find file ''C:\Program Files (x86)\IIS Express\MyPDF20171125144712173.pdf''.', N'   at System.Net.WebClient.DownloadFile(Uri address, String fileName)
   at System.Net.WebClient.DownloadFile(String address, String fileName)
   at ExTStudio.AllClasses.ComanClass.SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName)
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e)', N'System', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T14:48:15.640' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10051, N'1000', N'System.Net.Mail.SmtpException', N'The SMTP server requires a secure connection or the client was not authenticated. The server response was: 5.5.1 Authentication Required. Learn more at', N'   at System.Net.Mail.MailCommand.CheckResponse(SmtpStatusCode statusCode, String response)
   at System.Net.Mail.MailCommand.Send(SmtpConnection conn, Byte[] command, MailAddress from, Boolean allowUnicode)
   at System.Net.Mail.SmtpTransport.SendMail(MailAddress sender, MailAddressCollection recipients, String deliveryNotify, Boolean allowUnicode, SmtpFailedRecipientException& exception)
   at System.Net.Mail.SmtpClient.Send(MailMessage message)
   at ExTStudio.AllClasses.ComanClass.SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\AllClasses\ComanClass.cs:line 122
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 128', N'System', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T14:50:42.557' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10052, N'1000', N'System.Net.WebException', N'The process cannot access the file ''E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\PDFs\MyPDF20171125145736427.pdf'' because it is being used by another process.', N'   at System.Net.WebClient.DownloadFile(Uri address, String fileName)
   at System.Net.WebClient.DownloadFile(String address, String fileName)
   at ExTStudio.AllClasses.ComanClass.SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName)
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e)', N'System', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T15:02:04.627' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10053, N'1000', N'System.Net.Mail.SmtpException', N'The SMTP server requires a secure connection or the client was not authenticated. The server response was: 5.5.1 Authentication Required. Learn more at', N'   at System.Net.Mail.MailCommand.CheckResponse(SmtpStatusCode statusCode, String response)
   at System.Net.Mail.MailCommand.Send(SmtpConnection conn, Byte[] command, MailAddress from, Boolean allowUnicode)
   at System.Net.Mail.SmtpTransport.SendMail(MailAddress sender, MailAddressCollection recipients, String deliveryNotify, Boolean allowUnicode, SmtpFailedRecipientException& exception)
   at System.Net.Mail.SmtpClient.Send(MailMessage message)
   at ExTStudio.AllClasses.ComanClass.SendMailWithAttachments(String ToCustomerEmail, String Title, String subject, String Body, String Type, String FileName) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\AllClasses\ComanClass.cs:line 122
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 128', N'System', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-25T15:23:33.780' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10054, N'1000', N'System.IO.IOException', N'The process cannot access the file ''E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\PDFs\CustomSizeInfoPDFs\Agreement.pdf'' because it is being used by another process.', N'   at ExTStudio.Admin.membership_option_select.CreateMenCostumSizeInfoPDF() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 296
   at ExTStudio.Admin.membership_option_select.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 207', N'ExTStudio', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=6', N'A', CAST(N'2017-11-27T13:06:02.847' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10055, N'1000', N'System.IO.FileNotFoundException', N'Could not find file ''E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\PDFs\ReminderMsgPDFs\Manoj_ReminderMsg_20171127134318042.pdf''.', N'   at ExTStudio.Admin.reminder_message.CreateReminderMsgPDF(String Company_Name, String Company_Address, String Company_PhoneNo1, String Full_Name, String Booking_Date, String Total, String MobileNo, String Address)
   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e)', N'ExTStudio', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-27T13:44:04.240' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10056, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 133', N'ExTStudio', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-27T13:44:14.337' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10057, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 133', N'ExTStudio', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-27T13:44:17.303' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10058, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 133', N'ExTStudio', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-27T13:44:25.263' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10059, N'1000', N'System.NullReferenceException', N'Object reference not set to an instance of an object.', N'   at ExTStudio.Admin.reminder_message.lbtnDownload_Click(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\reminder-message.aspx.cs:line 133', N'ExTStudio', N'http://localhost:58352/Admin/reminder-message.aspx?MBookingId=1', N'A', CAST(N'2017-11-27T13:44:26.523' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10060, N'1000', N'System.ArgumentException', N'Column ''Belt'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.checkin_2.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\checkin-2.aspx.cs:line 81', N'System.Data', N'http://localhost:58352/Admin/checkin-2.aspx?ProbId=6', N'A', CAST(N'2017-11-27T17:59:43.307' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10061, N'1000', N'System.FormatException', N'String was not recognized as a valid DateTime.', N'   at System.DateTimeParse.Parse(String s, DateTimeFormatInfo dtfi, DateTimeStyles styles)
   at System.Convert.ToDateTime(String value)
   at ExTStudio.Admin.probetraining.ddlLocation_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\probetraining.aspx.cs:line 176', N'mscorlib', N'http://localhost:58352/Admin/probetraining.aspx', N'A', CAST(N'2017-11-29T16:32:25.113' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10062, N'1000', N'System.Data.SqlClient.SqlException', N'Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryHasMoreRows(Boolean& moreRows)
   at System.Data.SqlClient.SqlDataReader.TryReadInternal(Boolean setTimeout, Boolean& more)
   at System.Data.SqlClient.SqlDataReader.Read()
   at System.Data.Common.DataAdapter.FillLoadDataRow(SchemaMapping mapping)
   at System.Data.Common.DataAdapter.FillFromReader(DataSet dataset, DataTable datatable, String srcTable, DataReaderContainer dataReader, Int32 startRecord, Int32 maxRecords, DataColumn parentChapterColumn, Object parentChapterValue)
   at System.Data.Common.DataAdapter.Fill(DataSet dataSet, String srcTable, IDataReader dataReader, Int32 startRecord, Int32 maxRecords)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Membership_Select_By_Id_For_Trainer(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 943
   at ExTStudio.Admin.checkin_2.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\checkin-2.aspx.cs:line 68', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/checkin-2.aspx?ProbId=6', N'A', CAST(N'2017-11-29T16:53:35.153' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10063, N'1000', N'System.Data.SqlClient.SqlException', N'Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryHasMoreRows(Boolean& moreRows)
   at System.Data.SqlClient.SqlDataReader.TryReadInternal(Boolean setTimeout, Boolean& more)
   at System.Data.SqlClient.SqlDataReader.Read()
   at System.Data.Common.DataAdapter.FillLoadDataRow(SchemaMapping mapping)
   at System.Data.Common.DataAdapter.FillFromReader(DataSet dataset, DataTable datatable, String srcTable, DataReaderContainer dataReader, Int32 startRecord, Int32 maxRecords, DataColumn parentChapterColumn, Object parentChapterValue)
   at System.Data.Common.DataAdapter.Fill(DataSet dataSet, String srcTable, IDataReader dataReader, Int32 startRecord, Int32 maxRecords)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Membership_Select_By_Id_For_Trainer(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 943
   at ExTStudio.Admin.checkin_2.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\checkin-2.aspx.cs:line 68', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/checkin-2.aspx?ProbId=7', N'A', CAST(N'2017-11-29T16:54:13.740' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10064, N'1000', N'System.Data.SqlClient.SqlException', N'Subquery returned more than 1 value. This is not permitted when the subquery follows =, !=, <, <= , >, >= or when the subquery is used as an expression.', N'   at System.Data.SqlClient.SqlConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.SqlInternalConnection.OnError(SqlException exception, Boolean breakConnection, Action`1 wrapCloseInAction)
   at System.Data.SqlClient.TdsParser.ThrowExceptionAndWarning(TdsParserStateObject stateObj, Boolean callerHasConnectionLock, Boolean asyncClose)
   at System.Data.SqlClient.TdsParser.TryRun(RunBehavior runBehavior, SqlCommand cmdHandler, SqlDataReader dataStream, BulkCopySimpleResultSet bulkCopyHandler, TdsParserStateObject stateObj, Boolean& dataReady)
   at System.Data.SqlClient.SqlDataReader.TryHasMoreRows(Boolean& moreRows)
   at System.Data.SqlClient.SqlDataReader.TryReadInternal(Boolean setTimeout, Boolean& more)
   at System.Data.SqlClient.SqlDataReader.Read()
   at System.Data.Common.DataAdapter.FillLoadDataRow(SchemaMapping mapping)
   at System.Data.Common.DataAdapter.FillFromReader(DataSet dataset, DataTable datatable, String srcTable, DataReaderContainer dataReader, Int32 startRecord, Int32 maxRecords, DataColumn parentChapterColumn, Object parentChapterValue)
   at System.Data.Common.DataAdapter.Fill(DataSet dataSet, String srcTable, IDataReader dataReader, Int32 startRecord, Int32 maxRecords)
   at System.Data.Common.DbDataAdapter.FillInternal(DataSet dataset, DataTable[] datatables, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet, Int32 startRecord, Int32 maxRecords, String srcTable, IDbCommand command, CommandBehavior behavior)
   at System.Data.Common.DbDataAdapter.Fill(DataSet dataSet)
   at DAL.DBInteraction.GetDataPara(String s, SqlParameter[] ParaColl) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\DBInteraction.cs:line 64
   at DAL.InteractionMethod.Membership_Select_By_Id_For_Trainer(Prop objValue) in E:\Projects\ExTStudio Projects\ExTStudio\DAL\InteractionMethod.cs:line 943
   at ExTStudio.Admin.checkin_2.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\checkin-2.aspx.cs:line 68', N'.Net SqlClient Data Provider', N'http://localhost:58352/Admin/checkin-2.aspx?ProbId=7', N'A', CAST(N'2017-11-29T16:54:17.493' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10065, N'1000', N'System.IndexOutOfRangeException', N'Cannot find table 3.', N'   at System.Data.DataTableCollection.get_Item(Int32 index)
   at ExTStudio.Admin.membership.BindMember() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 73', N'System.Data', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:27:22.270' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10066, N'1000', N'System.IndexOutOfRangeException', N'Cannot find table 3.', N'   at System.Data.DataTableCollection.get_Item(Int32 index)
   at ExTStudio.Admin.membership.BindMember() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 73', N'System.Data', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:27:35.627' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10067, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 292', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:35:47.170' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10068, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 292', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:35:56.493' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10069, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 292', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:36:01.140' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10070, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 292', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:36:12.983' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10071, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e)', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:36:59.457' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10072, N'1000', N'System.ArgumentException', N'Column ''Trainer_Id'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e)', N'System.Data', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:38:25.267' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10073, N'1000', N'System.ArgumentOutOfRangeException', N'''ddlTrainingTimes'' has a SelectedValue which is invalid because it does not exist in the list of items.
Parameter name: value', N'   at System.Web.UI.WebControls.ListControl.set_SelectedValue(String value)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 292', N'System.Web', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:38:35.760' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10074, N'1000', N'System.ArgumentException', N'Column ''Course_Name'' does not belong to table Table.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e)', N'System.Data', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T13:41:05.693' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10075, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:19:26.287' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10076, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:22:24.163' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10077, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:23:56.467' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10078, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:24:45.723' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10079, N'1000', N'System.FormatException', N'Input string was not in a correct format.', N'   at System.Number.StringToNumber(String str, NumberStyles options, NumberBuffer& number, NumberFormatInfo info, Boolean parseDecimal)
   at System.Number.ParseInt64(String value, NumberStyles options, NumberFormatInfo numfmt)
   at System.Convert.ToInt64(String value)
   at ExTStudio.Admin.membership_option_select.ddlMOption_SelectedIndexChanged(Object sender, EventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 220', N'mscorlib', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=10', N'A', CAST(N'2017-11-30T14:25:02.057' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10080, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:29:20.457' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10081, N'1000', N'System.ArgumentException', N'Column ''Starter_Price'' does not belong to table Table1.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.membership_option_select.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 121', N'System.Data', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=10', N'A', CAST(N'2017-11-30T14:29:23.590' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10082, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T14:31:31.560' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10083, N'1000', N'System.ArgumentException', N'Column ''Starter_Price'' does not belong to table Table1.', N'   at System.Data.DataRow.GetDataColumn(String columnName)
   at System.Data.DataRow.get_Item(String columnName)
   at ExTStudio.Admin.membership_option_select.BindData() in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership-option-select.aspx.cs:line 123', N'System.Data', N'http://localhost:58352/Admin/membership-option-select.aspx?MOptionId=1', N'A', CAST(N'2017-11-30T14:31:32.863' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10084, N'1000', N'System.IndexOutOfRangeException', N'Index was outside the bounds of the array.', N'   at ExTStudio.Admin.location.Page_Load(Object sender, EventArgs e)', N'ExTStudio', N'http://localhost:58352/Admin/location.aspx', N'A', CAST(N'2017-11-30T15:03:13.523' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10085, N'1000', N'System.IndexOutOfRangeException', N'Index was outside the bounds of the array.', N'   at ExTStudio.Admin.location.Page_Load(Object sender, EventArgs e)', N'ExTStudio', N'http://localhost:58352/Admin/location.aspx', N'A', CAST(N'2017-11-30T15:03:31.940' AS DateTime))
INSERT [dbo].[tbl_Exception] ([Id], [ErrorStatusCode], [ExcType], [ExceptionMsg], [ExTrace], [ExSource], [WebURL], [Flag], [CreateDate]) VALUES (10086, N'1000', N'System.Threading.ThreadAbortException', N'Thread was being aborted.', N'   at System.Threading.Thread.AbortInternal()
   at System.Threading.Thread.Abort(Object stateInfo)
   at System.Web.HttpResponse.AbortCurrentThread()
   at System.Web.HttpResponse.End()
   at System.Web.HttpResponse.Redirect(String url, Boolean endResponse, Boolean permanent)
   at System.Web.HttpResponse.Redirect(String url)
   at ExTStudio.Admin.membership.repMember_ItemCommand(Object sender, ListViewCommandEventArgs e) in E:\Projects\ExTStudio Projects\ExTStudio\ExTStudio\Admin\membership.aspx.cs:line 333', N'mscorlib', N'http://localhost:58352/Admin/membership.aspx', N'A', CAST(N'2017-11-30T15:40:43.080' AS DateTime))
SET IDENTITY_INSERT [dbo].[tbl_Exception] OFF
SET IDENTITY_INSERT [dbo].[tbl_Location_Mst] ON 

INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, NULL, NULL, NULL, N'dfsfsdf', N'sdfsdfsdf', CAST(2134564565 AS Numeric(18, 0)), N'~/images/Location/pdgicon.png', N'A', CAST(N'2017-11-16T19:39:44.283' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, NULL, NULL, NULL, N'dfsfsdf', N'sdfsdfsdf', CAST(2134564565 AS Numeric(18, 0)), N'~/images/Location/pdgicon.png', N'A', CAST(N'2017-11-16T19:43:57.847' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, NULL, NULL, NULL, N'fyfy', N'ftyftyfy', CAST(3621312312 AS Numeric(18, 0)), N'~/images/no-image.png', N'A', CAST(N'2017-11-16T20:02:44.150' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, NULL, NULL, NULL, N'jhljl', N'jljl', CAST(2134313331 AS Numeric(18, 0)), N'~/images/no-image.png', N'A', CAST(N'2017-11-17T10:33:01.370' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, NULL, NULL, NULL, N'hjkhk', N'hjkhjk', CAST(1231231231 AS Numeric(18, 0)), N'~/images/no-image.png', N'A', CAST(N'2017-11-17T10:34:34.887' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (6, N'MON,TUES,SAT', N'D', CAST(N'2017-11-18T16:47:21.957' AS DateTime), N'pdg', N'zsdfsdfsdf', CAST(1234566456 AS Numeric(18, 0)), N'~/images/Location/DefaultFile.png', N'A', CAST(N'2017-11-18T16:47:21.997' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Location_Mst] ([Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Location_Name], [Address], [MobileNo], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (7, N'MON', N'D', CAST(N'2017-11-29T15:15:06.970' AS DateTime), N'Ahmedabad', N'aaaa', CAST(9876543210 AS Numeric(18, 0)), NULL, N'A', CAST(N'2017-11-29T15:15:09.310' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Location_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Membership_Mst] ON 

INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'../images/no-image.png', 1, 1, N'Manoj', N'asdfda asd', CAST(645967 AS Numeric(18, 0)), N'India', CAST(4567896456 AS Numeric(18, 0)), N'excelsiordevlopers@gmail.com', N'sdf', 1, N'A', CAST(N'2017-11-24T11:00:29.723' AS DateTime), CAST(N'2017-11-30T14:34:53.380' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'../images/no-image.png', 2, 1, N'Rajesh', N'dasda', CAST(456465 AS Numeric(18, 0)), N'India', CAST(8974564656 AS Numeric(18, 0)), NULL, N'54', NULL, N'A', CAST(N'2017-11-24T11:59:02.540' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'../images/no-image.png', 1, 1, N'Kishor', N'asd', CAST(121456 AS Numeric(18, 0)), N'India', CAST(8974654654 AS Numeric(18, 0)), NULL, N'asd', NULL, N'A', CAST(N'2017-11-24T12:10:54.350' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, N'../images/no-image.png', 1, 1, N'sdf', N'sdf', CAST(231231 AS Numeric(18, 0)), N'India', CAST(7895645645 AS Numeric(18, 0)), NULL, N'6646', NULL, N'A', CAST(N'2017-11-25T11:20:36.050' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, N'../images/Members/no-image.png', 1, 1, N'sdf', N'sdf', CAST(879456 AS Numeric(18, 0)), N'India', CAST(8798797897 AS Numeric(18, 0)), NULL, N'dgs', NULL, N'A', CAST(N'2017-11-25T12:24:26.610' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (6, N'../images/Members/download (3).jpg', 1, 1, N'samir', N'asd', CAST(546546 AS Numeric(18, 0)), N'India', CAST(8979849879 AS Numeric(18, 0)), N'samir@gmail.com', N'werwerw', NULL, N'A', CAST(N'2017-11-27T12:06:20.230' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (7, N'../images/Members/download.jpg', 1, 1, N'Montu Patel', N'sfsdf sf dgdsgd', CAST(789787 AS Numeric(18, 0)), N'India', CAST(7897645654 AS Numeric(18, 0)), N'montu@gmail.com', N'dfgdf', NULL, N'A', CAST(N'2017-11-27T18:04:23.070' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (8, N'../images/no-image.png', 2, 1, N'Mahesh', N'sadf', CAST(789797 AS Numeric(18, 0)), N'India', CAST(8796456456 AS Numeric(18, 0)), N'mahesh@gmail.com', N'8796', NULL, N'A', CAST(N'2017-11-30T11:20:41.290' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (10, N'../Images/Members/Abc879546213_Members.jpg', 1, 1, N'Abc', N'sad', CAST(789456 AS Numeric(18, 0)), N'India', CAST(879546213 AS Numeric(18, 0)), N'aaa@gmail.com', N'213', 1, N'A', CAST(N'2017-11-30T12:07:55.983' AS DateTime), CAST(N'2017-11-30T12:09:39.063' AS DateTime), 1, 0)
INSERT [dbo].[tbl_Membership_Mst] ([Member_Id], [ImagePath], [TTime_Id], [Trainer_Id], [Student_Name], [Address], [Zip_Code], [Country], [MobileNo], [Email_Id], [IBAN], [M_Option_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (11, N'../images/no-image.png', 1, 1, N'dfg', N'dfg', CAST(443454 AS Numeric(18, 0)), N'India', CAST(7894654564 AS Numeric(18, 0)), N'sdff@gmail.com', N'ghsgf', NULL, N'A', CAST(N'2017-11-30T13:23:33.783' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Membership_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Membership_Options_Charges] ON 

INSERT [dbo].[tbl_Membership_Options_Charges] ([M_Option_Id], [Is_Kide_Adult], [Starter_Price], [Package_Name], [Package_Time], [Times_Per_Week], [Monthly_Investment], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'K', CAST(150.00 AS Numeric(18, 2)), N'Panda', N'12 Months', N'79', CAST(49.00 AS Numeric(18, 2)), N'A', CAST(N'2017-11-27T12:07:39.893' AS DateTime), CAST(N'2017-11-30T12:11:18.433' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Membership_Options_Charges] ([M_Option_Id], [Is_Kide_Adult], [Starter_Price], [Package_Name], [Package_Time], [Times_Per_Week], [Monthly_Investment], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'K', CAST(12.00 AS Numeric(18, 2)), N'fds', N'sdf', N'2', CAST(12.00 AS Numeric(18, 2)), N'A', CAST(N'2017-11-29T16:55:30.187' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Options_Charges] ([M_Option_Id], [Is_Kide_Adult], [Starter_Price], [Package_Name], [Package_Time], [Times_Per_Week], [Monthly_Investment], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'A', CAST(10.00 AS Numeric(18, 2)), N'asd', N'asd', N'10', CAST(10.00 AS Numeric(18, 2)), N'A', CAST(N'2017-11-29T17:14:04.433' AS DateTime), CAST(N'2017-11-30T12:12:10.707' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[tbl_Membership_Options_Charges] OFF
SET IDENTITY_INSERT [dbo].[tbl_Membership_Product_Mst] ON 

INSERT [dbo].[tbl_Membership_Product_Mst] ([M_Product_Id], [M_Option_Id], [Product_Name], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 1, N'abc', N'../images/MemberProduct/download (1).jpg', N'A', CAST(N'2017-11-27T12:07:58.400' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Product_Mst] ([M_Product_Id], [M_Option_Id], [Product_Name], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, 1, N'xyz', N'../images/MemberProduct/DefaultFile1.png', N'A', CAST(N'2017-11-27T12:08:05.807' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Membership_Product_Mst] ([M_Product_Id], [M_Option_Id], [Product_Name], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, 1, N'Abd', N'../images/no-image.png', N'A', CAST(N'2017-11-29T17:19:58.590' AS DateTime), CAST(N'2017-11-30T12:13:44.390' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[tbl_Membership_Product_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Message_Mst] ON 

INSERT [dbo].[tbl_Message_Mst] ([Message_Id], [Message], [BVK], [Users], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'masdd', N'sadf', N'jd', N'A', CAST(N'2017-11-17T19:30:52.717' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Message_Mst] ([Message_Id], [Message], [BVK], [Users], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'masdd', N'sadf', N'jd', N'A', CAST(N'2017-11-17T19:31:50.700' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Message_Mst] ([Message_Id], [Message], [BVK], [Users], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'masdd', N'sadf', N'jd', N'A', CAST(N'2017-11-17T19:32:32.637' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Message_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Monthly_Bookings_Mst] ON 

INSERT [dbo].[tbl_Monthly_Bookings_Mst] ([M_Booking_Id], [Booking_Date], [Probetraining_Id], [Joining_Fee], [Monthly_Booking], [IBAN], [Total], [IsPaid], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, CAST(N'2017-11-18T15:34:56.897' AS DateTime), 1, CAST(50.00 AS Numeric(18, 2)), CAST(2.00 AS Numeric(18, 2)), N'assd', CAST(52.00 AS Numeric(18, 2)), N'A', N'D', CAST(N'2017-11-18T15:34:56.903' AS DateTime), CAST(N'2017-11-30T12:15:36.150' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Monthly_Bookings_Mst] ([M_Booking_Id], [Booking_Date], [Probetraining_Id], [Joining_Fee], [Monthly_Booking], [IBAN], [Total], [IsPaid], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, CAST(N'2017-11-18T15:41:10.240' AS DateTime), 1, CAST(500.00 AS Numeric(18, 2)), CAST(20.00 AS Numeric(18, 2)), N'adsad', CAST(520.00 AS Numeric(18, 2)), N'D', N'D', CAST(N'2017-11-18T15:41:10.243' AS DateTime), CAST(N'2017-11-18T16:00:02.337' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Monthly_Bookings_Mst] ([M_Booking_Id], [Booking_Date], [Probetraining_Id], [Joining_Fee], [Monthly_Booking], [IBAN], [Total], [IsPaid], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, CAST(N'2017-11-18T16:04:34.760' AS DateTime), 1, CAST(500.00 AS Numeric(18, 2)), CAST(5.00 AS Numeric(18, 2)), N'gsdf', CAST(505.00 AS Numeric(18, 2)), N'D', N'D', CAST(N'2017-11-18T16:04:34.763' AS DateTime), CAST(N'2017-11-29T16:29:58.127' AS DateTime), 1, 1)
INSERT [dbo].[tbl_Monthly_Bookings_Mst] ([M_Booking_Id], [Booking_Date], [Probetraining_Id], [Joining_Fee], [Monthly_Booking], [IBAN], [Total], [IsPaid], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, CAST(N'2017-11-24T00:00:00.000' AS DateTime), 1, CAST(200.00 AS Numeric(18, 2)), CAST(50.00 AS Numeric(18, 2)), N'asdad', CAST(250.00 AS Numeric(18, 2)), N'D', N'A', CAST(N'2017-11-24T11:24:17.383' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Monthly_Bookings_Mst] ([M_Booking_Id], [Booking_Date], [Probetraining_Id], [Joining_Fee], [Monthly_Booking], [IBAN], [Total], [IsPaid], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, CAST(N'2017-12-20T00:00:00.000' AS DateTime), 1, CAST(10.00 AS Numeric(18, 2)), CAST(10.00 AS Numeric(18, 2)), N'sdf', CAST(20.00 AS Numeric(18, 2)), N'D', N'A', CAST(N'2017-11-29T18:21:03.190' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Monthly_Bookings_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Probetraining_Mst] ON 

INSERT [dbo].[tbl_Probetraining_Mst] ([Probetraining_Id], [Location_Id], [Trainer_Id], [Full_Name], [Email_Id], [MobileNo], [Age], [Heard], [GoogleFacebook], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 1, 1, N'asdas', N'jd@gmail.com', CAST(2354645646 AS Numeric(18, 0)), 25, N'ssfsf', N'sfsfsfsf', N'A', CAST(N'2017-11-17T20:22:25.830' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Probetraining_Mst] ([Probetraining_Id], [Location_Id], [Trainer_Id], [Full_Name], [Email_Id], [MobileNo], [Age], [Heard], [GoogleFacebook], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, 3, 1, N'Rajkumar', N'Rajkumar@gmail.com', CAST(9874641233 AS Numeric(18, 0)), 20, N'sadas', N'sadasdad', N'A', CAST(N'2017-11-18T19:06:04.567' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Probetraining_Mst] ([Probetraining_Id], [Location_Id], [Trainer_Id], [Full_Name], [Email_Id], [MobileNo], [Age], [Heard], [GoogleFacebook], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, 6, 1, N'vikram prajapati', N'vikram@gmail.com', CAST(2135464564 AS Numeric(18, 0)), 25, N'MON,TUES', N'Google', N'A', CAST(N'2017-11-21T15:52:29.997' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Probetraining_Mst] ([Probetraining_Id], [Location_Id], [Trainer_Id], [Full_Name], [Email_Id], [MobileNo], [Age], [Heard], [GoogleFacebook], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, 6, 1, N'Kiran Patel', N'excelsiordevlopers@gmail.com', CAST(8797645645 AS Numeric(18, 0)), 25, N'MON,TUES', N'Facebook', N'A', CAST(N'2017-11-21T16:00:05.940' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Probetraining_Mst] ([Probetraining_Id], [Location_Id], [Trainer_Id], [Full_Name], [Email_Id], [MobileNo], [Age], [Heard], [GoogleFacebook], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, 1, 1, N'Aaaa Bbbbb', N'aaa@gmail.com', CAST(9876543210 AS Numeric(18, 0)), 20, N'MON', N'Google', N'A', CAST(N'2017-11-29T16:48:20.827' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Probetraining_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Product_Mst] ON 

INSERT [dbo].[tbl_Product_Mst] ([Product_Id], [Product_Name], [Size], [Price], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'abc', N'small', CAST(25 AS Numeric(18, 0)), N'/images/Product/DefaultFile.png', N'A', CAST(N'2017-11-16T18:10:45.990' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Product_Mst] ([Product_Id], [Product_Name], [Size], [Price], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'xya', N'Large', CAST(30 AS Numeric(18, 0)), N'/images/Product/DefaultFile1.png', N'A', CAST(N'2017-11-16T18:13:55.220' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Product_Mst] ([Product_Id], [Product_Name], [Size], [Price], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, N'dgdfg', N'gdg', CAST(123 AS Numeric(18, 0)), N'~/images/no-image.png', N'A', CAST(N'2017-11-16T18:18:29.823' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Product_Mst] ([Product_Id], [Product_Name], [Size], [Price], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, N'BB', N'Small', CAST(5 AS Numeric(18, 0)), N'../Images/Product/BB_Product.jpg', N'A', CAST(N'2017-11-29T14:49:21.230' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Product_Mst] ([Product_Id], [Product_Name], [Size], [Price], [ImagePath], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, N'BB', N'Small', CAST(5 AS Numeric(18, 0)), N'../Images/Product/BB_Product.jpg', N'A', CAST(N'2017-11-29T14:50:55.577' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Product_Mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Registration] ON 

INSERT [dbo].[tbl_Registration] ([Regi_Id], [Full_Name], [Shcool_Name], [Address], [Zip_Code], [Country], [Email_Id], [MobileNo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, NULL, N'abc', N'asda asd ad', CAST(123132 AS Numeric(18, 0)), N'India', N'admin@gmail.com', CAST(9876543210 AS Numeric(18, 0)), N'A', CAST(N'2017-11-16T17:00:35.070' AS DateTime), NULL, 0, NULL)
INSERT [dbo].[tbl_Registration] ([Regi_Id], [Full_Name], [Shcool_Name], [Address], [Zip_Code], [Country], [Email_Id], [MobileNo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, NULL, N'dfgdfg', N'dfgdfgdfg', CAST(213123 AS Numeric(18, 0)), N'gufgugh', N'krisha@gmail.com', CAST(1312313123 AS Numeric(18, 0)), N'A', CAST(N'2017-11-16T19:56:19.890' AS DateTime), NULL, 0, NULL)
INSERT [dbo].[tbl_Registration] ([Regi_Id], [Full_Name], [Shcool_Name], [Address], [Zip_Code], [Country], [Email_Id], [MobileNo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, NULL, N'asd', N'asd', CAST(534534 AS Numeric(18, 0)), N'fsfs', N'fssdfsf@gmial.com', CAST(5345345345 AS Numeric(18, 0)), N'A', CAST(N'2017-11-17T11:01:17.797' AS DateTime), NULL, 0, NULL)
INSERT [dbo].[tbl_Registration] ([Regi_Id], [Full_Name], [Shcool_Name], [Address], [Zip_Code], [Country], [Email_Id], [MobileNo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, NULL, NULL, NULL, CAST(0 AS Numeric(18, 0)), NULL, NULL, CAST(0 AS Numeric(18, 0)), N'A', CAST(N'2017-11-17T12:48:13.207' AS DateTime), NULL, 0, NULL)
INSERT [dbo].[tbl_Registration] ([Regi_Id], [Full_Name], [Shcool_Name], [Address], [Zip_Code], [Country], [Email_Id], [MobileNo], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (10004, NULL, N'Abc', N'xyz', CAST(123456 AS Numeric(18, 0)), N'India', N'abc@gmial.com', CAST(9874564123 AS Numeric(18, 0)), N'A', CAST(N'2017-11-24T10:42:44.733' AS DateTime), NULL, 0, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Registration] OFF
SET IDENTITY_INSERT [dbo].[tbl_Trainer_mst] ON 

INSERT [dbo].[tbl_Trainer_mst] ([Trainer_Id], [ImagePath], [Full_Name], [Email_Id], [See], [Do], [Admin_Id], [MobileNo], [Password], [About], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, N'~/images/Trainer/download.jpg', N'Paresh', N'paresh@gmail.com', N'1', N'2', 1, CAST(9876541236 AS Numeric(18, 0)), N'123', N'sdfs fsdfsdf sdf sdffsf sf sdfsd f', N'A', CAST(N'2017-11-17T15:20:42.353' AS DateTime), NULL, 1, 1)
INSERT [dbo].[tbl_Trainer_mst] ([Trainer_Id], [ImagePath], [Full_Name], [Email_Id], [See], [Do], [Admin_Id], [MobileNo], [Password], [About], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, N'../Images/Trainer/Vivek_Patel_19874563211_Trainer.jpg', N'Vivek Patel', N'vivek@gmail.com', N'a', N'a', 1, CAST(9874563211 AS Numeric(18, 0)), N'123', N'fhfd fghfhfhf', N'A', CAST(N'2017-11-29T15:25:34.060' AS DateTime), CAST(N'2017-11-30T12:18:43.120' AS DateTime), 1, 1)
SET IDENTITY_INSERT [dbo].[tbl_Trainer_mst] OFF
SET IDENTITY_INSERT [dbo].[tbl_Training_Timings] ON 

INSERT [dbo].[tbl_Training_Timings] ([TTime_Id], [Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Course_Id], [Course_Form], [Course_To], [AddOnce], [EveryWeek], [Trainer_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (1, 1, N'', N'A', CAST(N'2017-11-19T00:00:00.000' AS DateTime), 1, N'1 pm', N'12 am', N'1', N'3', 1, N'A', CAST(N'2017-11-17T17:06:39.807' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Training_Timings] ([TTime_Id], [Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Course_Id], [Course_Form], [Course_To], [AddOnce], [EveryWeek], [Trainer_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (2, 1, N'MON,TUES,WED', N'D', CAST(N'2017-11-17T17:13:47.670' AS DateTime), 1, N'1', N'2', N'14', N'23', 1, N'A', CAST(N'2017-11-17T17:13:49.973' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Training_Timings] ([TTime_Id], [Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Course_Id], [Course_Form], [Course_To], [AddOnce], [EveryWeek], [Trainer_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (3, 1, N'MON', N'D', CAST(N'2017-11-21T11:10:24.847' AS DateTime), 1, N'10:00', N'01:00', N'1', N'3', 1, N'A', CAST(N'2017-11-21T11:10:24.853' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Training_Timings] ([TTime_Id], [Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Course_Id], [Course_Form], [Course_To], [AddOnce], [EveryWeek], [Trainer_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (4, 1, N'MON', N'D', CAST(N'2017-11-29T16:27:09.180' AS DateTime), 1, N'10 PM', N'11 PM', N'1', N'3', 1, N'A', CAST(N'2017-11-29T16:27:11.073' AS DateTime), NULL, 1, NULL)
INSERT [dbo].[tbl_Training_Timings] ([TTime_Id], [Location_Id], [T_Day], [Is_Holiday], [Holiday_Date], [Course_Id], [Course_Form], [Course_To], [AddOnce], [EveryWeek], [Trainer_Id], [Flag], [CreateDate], [UpdateDate], [CreateUser], [UpdateUser]) VALUES (5, 1, N'MON', N'D', CAST(N'2017-11-30T11:51:00.900' AS DateTime), 1, N'10 PM', N'11 PM', N'1', N'3', 1, N'A', CAST(N'2017-11-30T11:51:00.920' AS DateTime), NULL, 1, NULL)
SET IDENTITY_INSERT [dbo].[tbl_Training_Timings] OFF
/****** Object:  StoredProcedure [dbo].[SP_Admin_Login]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Admin_Login]
		@Admin_Login_Id	bigint =null,
		@Regi_Id	bigint	=null,
		@UserName	nvarchar(50) =null,
		@Email_Id	nvarchar(150) =null,
		@Password	nvarchar(50) =null,
		@NewPassword	nvarchar(50) =null,
		@IsTrainer	nchar(1) =null,
		@Flag	nchar(1) =null,
		@CreateDate	datetime =null,
		@UpdateDate	datetime =null,
		@CreateUser	bigint =null,
		@UpdateUser	bigint =null,
		@intOutput nvarchar(50)=null OUTPUT,
		@NumRowsChanged nvarchar(50)=null OUTPUT,
		@ErrorCode nvarchar(50)=null OUTPUT,
		@Mode varchar(50) =NULL
	
AS
BEGIN

	If @Mode = 'SelectById'
	BEGIN
		select * ,'A' as Role from tbl_Admin_Login where Admin_Login_Id=@Admin_Login_Id  and Flag='A' 
	END

	If @Mode = 'SelectByEmialAndPassword'
	BEGIN
		select * ,'A' as Role from tbl_Admin_Login where Email_Id=@Email_Id and Password=@Password and Flag='A' 
	END

	If @Mode = 'SelectByEmialAndPasswordTrainer'
	BEGIN
		select * ,'T' as Role,Trainer_Id as Admin_Login_Id  from tbl_Trainer_mst where Email_Id=@Email_Id and Password=@Password and Flag='A' 
	END

	Else IF @Mode = 'ChangePassword'
	BEGIN
		update tbl_Admin_Login set Password = @NewPassword,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser where Admin_Login_Id = @Admin_Login_Id and Password=@Password
	END

	Else IF @Mode = 'ChangePasswordTrainer'
	BEGIN
		update tbl_Trainer_mst set Password = @NewPassword,UpdateDate=@UpdateDate,UpdateUser=@UpdateUser where Trainer_Id = @Admin_Login_Id and Password=@Password
	END
	
	if @Mode = 'SelectByEmailId'
	Begin

		if @IsTrainer = 'D'
		begin
			select * from tbl_Admin_Login where Email_Id=@Email_Id
		 end
	 else
		 begin
			select *, Full_Name as UserName from tbl_Trainer_mst where Email_Id=@Email_Id
		 end
	End

END




GO
/****** Object:  StoredProcedure [dbo].[SP_Check_In_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Check_In_Mst]

	@CechkId	bigint	= null,
	@Probetraining_Id	bigint	= null,
	@Full_Name	nvarchar(100)	= null,
	@BOD	datetime	= null,
	@Belt	nvarchar(150)	= null,
	@NBTDate	datetime	= null,
	@NBTTime	nvarchar(50)	= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select CechkId, Probetraining_Id, Full_Name, BOD, Belt, NBTDate, NBTTime, Flag from tbl_Check_In_Mst Where Flag = 'A'
		
		Select CechkId, Probetraining_Id, Full_Name, BOD, Belt, NBTDate, NBTTime, Flag from tbl_Check_In_Mst Where Flag = 'D'
		
		Select CechkId, Probetraining_Id, Full_Name, BOD, Belt, NBTDate, NBTTime, Flag from tbl_Check_In_Mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select CechkId, Probetraining_Id, Full_Name, BOD, Belt, NBTDate, NBTTime, Flag from tbl_Check_In_Mst Where CechkId = @CechkId
	END

	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Check_In_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Check_In_Mst ( Probetraining_Id, Full_Name, BOD, Belt, NBTDate, NBTTime, Flag, CreateDate, CreateUser)
			              VALUES ( @Probetraining_Id, @Full_Name, @BOD, @Belt, @NBTDate, @NBTTime, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Check_In_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and CechkId<>@CechkId)
		BEGIN	Update tbl_Check_In_Mst set  Probetraining_Id=@Probetraining_Id, Full_Name=@Full_Name, BOD=@BOD, Belt=@Belt, NBTDate=@NBTDate, NBTTime=@NBTTime, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where CechkId=@CechkId
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Check_In_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where CechkId=@CechkId
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Company_Detail_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Company_Detail_Mst]
	@Company_Id	bigint =null,
	@Company_Name	nvarchar(100) =null,
	@Company_Short_Name	nvarchar(10) =null,
	@Company_Address_Line1	nvarchar(100) =null,
	@Company_Address_Line2	nvarchar(100) =null,
	@Company_Street	nvarchar(50)=null,
	@Company_Nearby	nvarchar(50)=null,
	@Company_Area	nvarchar(50)=null,
	@Company_City	nvarchar(50)=null,
	@Company_State	nvarchar(50)=null,
	@Company_Country	nvarchar(50)=null,
	@Company_Pincode	numeric(18, 0)=null,
	@Company_Email	nvarchar(50)=null,
	@Company_PhoneNo1	numeric(18, 0)=null,
	@Company_PhoneNo2	numeric(18, 0)=null,
	@Company_Website	nvarchar(50)=null,
	@Company_Logo	nvarchar(500)=null,
	@Flag	nchar(1)=null,
	@CreateUser	bigint=null,
	@UpdateUser	bigint=null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null

AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select * ,Company_Address_Line1 + ' '+ Company_Address_Line2 +' '+ Company_Street +' '+ Company_Nearby +' '+ Company_Area  +' '+ Company_City +' '+ Company_State  +' '+ Company_Country  as Address from tbl_Company_Detail_Mst Where Flag = 'A'
		Select * from tbl_Company_Detail_Mst Where Flag = 'D'
		Select * from tbl_Company_Detail_Mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select * from tbl_Company_Detail_Mst Where Company_Id = @Company_Id
	END

	IF @Mode='Insert'
	BEGIN
	
		INSERT INTO tbl_Company_Detail_Mst ( Company_Name, Company_Short_Name, Company_Address_Line1, Company_Address_Line2, Company_Street, Company_Nearby, Company_Area, Company_City, Company_State, Company_Country, Company_Pincode, Company_Email, Company_PhoneNo1, Company_PhoneNo2, Company_Website, Company_Logo, Flag, CreateDate, CreateUser)
			   VALUES ( @Company_Name, @Company_Short_Name, @Company_Address_Line1, @Company_Address_Line2, @Company_Street, @Company_Nearby, @Company_Area, @Company_City, @Company_State, @Company_Country, @Company_Pincode, @Company_Email, @Company_PhoneNo1, @Company_PhoneNo2, @Company_Website, @Company_Logo, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	END

	IF @Mode='Update'
	BEGIN
		Update tbl_Company_Detail_Mst set Company_Name=@Company_Name, Company_Short_Name=@Company_Short_Name, Company_Address_Line1=@Company_Address_Line1, Company_Address_Line2=@Company_Address_Line2, Company_Street=@Company_Street, Company_Nearby=@Company_Nearby, Company_Area=@Company_Area, Company_City=@Company_City, Company_State=@Company_State, Company_Country=@Company_Country, Company_Pincode=@Company_Pincode, Company_Email=@Company_Email, Company_PhoneNo1=@Company_PhoneNo1, Company_PhoneNo2=@Company_PhoneNo2, Company_Website=@Company_Website, Company_Logo=@Company_Logo, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Company_Id=@Company_Id
		
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Company_Detail_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Company_Id=@Company_Id
		
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END

	IF @Mode='SelectforHome'
	BEGIN
		Select Company_PhoneNo2, Company_Email, (Company_Address_Line1 + ' ' + Company_Address_Line2 + ' ' + Company_Street + ' ' +Company_Nearby + ' ' +Company_Area + ' ' + Company_City + ' ' +Company_State + ' ' + Company_Country + ' ' +CONVERT(nvarchar(50), Company_Pincode )) as Company_Address
		 from tbl_Company_Detail_Mst Where Flag='A'
		Select * from tbl_Social_Mst Where Flag='A'
	END
END


GO
/****** Object:  StoredProcedure [dbo].[SP_Course_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Course_Mst]
	@Course_Id	bigint = null,
	@Course_Name nvarchar(100) = null,
	@Location_Id bigint = null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select tc.Course_Id, tc.Course_Name, tc.Location_Id, tc.Flag, tl.Location_Name from tbl_Course_Mst as tc
		inner join tbl_Location_Mst as tl on tc.Location_Id = tl.Location_Id
		Where tc.Flag = 'A'
		
		Select tc.Course_Id, tc.Course_Name, tc.Location_Id, tc.Flag, tl.Location_Name from tbl_Course_Mst as tc
		inner join tbl_Location_Mst as tl on tc.Location_Id = tl.Location_Id
		Where tc.Flag = 'D'
		
		Select tc.Course_Id, tc.Course_Name, tc.Location_Id, tc.Flag, tl.Location_Name from tbl_Course_Mst as tc
		inner join tbl_Location_Mst as tl on tc.Location_Id = tl.Location_Id
	END

	IF @Mode='SelectById'
	BEGIN
		Select Course_Id, Course_Name, Location_Id, Flag from tbl_Course_Mst Where Course_Id = @Course_Id
	END

	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Course_Mst where Course_Name= @Course_Name)
		BEGIN
		INSERT INTO tbl_Course_Mst (Course_Name, Location_Id, Flag, CreateDate, CreateUser)
			              VALUES ( @Course_Name, @Location_Id, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END


	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Course_Mst where Course_Name= @Course_Name and Course_Id<>@Course_Id)
		BEGIN	Update tbl_Course_Mst set Course_Name=@Course_Name, Location_Id=@Location_Id, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Course_Id=@Course_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
			BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Course_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Course_Id=@Course_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_EmailConfigration_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_EmailConfigration_Mst]
	@ID	bigint	=NULL ,
	@Host	nvarchar(MAX)	=NULL ,
	@Port	numeric(18, 0)	=NULL ,
	@UserName	nvarchar(MAX)	=NULL ,
	@Password	nvarchar(MAX)	=NULL ,
	@SSL	bit	=NULL ,
	@EmailType	nvarchar(MAX)	=NULL ,

	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode nvarchar(50) = null
AS
BEGIN
	if @Mode = 'SelectAll'
	Begin
		select * from tbl_EmailConfigration_Mst 
	End

	if @Mode = 'SelectByID'
	Begin
		select * from tbl_EmailConfigration_Mst where ID=@ID
	End

	if @Mode = 'Insert'
	Begin
		if not exists (select * from tbl_EmailConfigration_Mst where Host=@Host and Port=@Port and UserName=@UserName and Password=@Password and SSL=@SSL and EmailType=@EmailType)
		begin
		insert into tbl_EmailConfigration_Mst(Host, Port, UserName, Password, SSL, EmailType)
		values (@Host, @Port, @UserName, @Password, @SSL, @EmailType)
			SET @intOutput = 'I';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		end
	else
		begin
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR 
		end
	End

	if @Mode = 'Update'
	Begin
	if not exists (select * from tbl_EmailConfigration_Mst where Host=@Host and Port=@Port and UserName=@UserName and Password=@Password and SSL=@SSL and EmailType=@EmailType and ID<>@ID)
		Begin
		update tbl_EmailConfigration_Mst set Host=@Host, Port=@Port, UserName=@UserName, Password=@Password, SSL=@SSL, EmailType=@EmailType where ID=@ID
			SET @intOutput = 'U';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		End
	End

	if @Mode = 'Delete'
	Begin
	
		delete from tbl_EmailConfigration_Mst where ID=@ID
			SET @intOutput = 'U';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	End

	if @Mode = 'SelectByEmailType'
	Begin
		select * from tbl_EmailConfigration_Mst where EmailType=@EmailType
	End
END



















GO
/****** Object:  StoredProcedure [dbo].[SP_Exception]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Exception]
	@Id	bigint =null,
	@ErrorStatusCode	nvarchar(MAX)=null,
	@ExcType	nvarchar(MAX)=null,
	@ExceptionMsg	nvarchar(MAX)=null,
	@ExTrace	nvarchar(MAX)=null,
	@ExSource	nvarchar(MAX)=null,
	@Flag	nchar(1)=null,
	@WebURL nvarchar(MAX)=null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode nvarchar(50)=null
AS
BEGIN
	If @Mode = 'Insert'
	Begin
		INSERT INTO tbl_Exception ( ErrorStatusCode, ExcType, ExceptionMsg, ExTrace, ExSource, Flag, CreateDate,WebURL) Values ( @ErrorStatusCode, @ExcType, @ExceptionMsg, @ExTrace, @ExSource, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')),@WebURL)
	End

--Else If @Mode = 'Update'
--	Begin
--		UPDATE db_owner.tblEvent
--		SET  Title = @Title,Discription = @Discription, UploadDate=@UploadDate  WHERE Id=@Id
--	End

Else If @Mode = 'Delete'
	Begin
		DELETE FROM tbl_Exception WHERE Id=@Id
	End

Else If @Mode = 'SelectAll'
	Begin
		SELECT * FROM tbl_Exception order by Id desc
		SELECT Top 1 Id,ErrorStatusCode,ExcType,ExceptionMsg,ExTrace,ExSource,WebURL FROM tbl_Exception order by Id desc
	End

Else If @Mode = 'SelectById'
	Begin
		SELECT * FROM tbl_Exception WHERE Id=@Id
	End
 
END




GO
/****** Object:  StoredProcedure [dbo].[SP_Location_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Location_Mst]  
	@Location_Id	bigint	= null,
	@T_Day	nvarchar(100)	= null,
	@Is_Holiday	nchar(1)	= null,
	@Holiday_Date	datetime	= null,
	@Location_Name	nvarchar(100)	= null,
	@Address	nvarchar(MAX)	= null,
	@MobileNo	numeric(18, 0)	= null,
	@ImagePath	nvarchar(MAX)= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Location_Id,T_Day, Is_Holiday, Holiday_Date, Location_Name, Address, MobileNo, ImagePath, Flag from tbl_Location_Mst Where Flag = 'A'
		
		Select Location_Id, T_Day, Is_Holiday, Holiday_Date,Location_Name, Address, MobileNo, ImagePath, Flag from tbl_Location_Mst Where Flag = 'D'
		
		Select Location_Id, T_Day, Is_Holiday, Holiday_Date,Location_Name, Address, MobileNo, ImagePath, Flag from tbl_Location_Mst 
	
		Select Trainer_Id, ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About from tbl_Trainer_mst Where Flag = 'A'
	
	END

	IF @Mode='SelectById'
	BEGIN
		Select Location_Id,T_Day, Is_Holiday, Holiday_Date, Location_Name, Address, MobileNo, ImagePath, Flag from tbl_Location_Mst Where Location_Id = @Location_Id
	END

	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Location_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Location_Mst (T_Day, Is_Holiday, Holiday_Date, Location_Name, Address, MobileNo, ImagePath, Flag, CreateDate, CreateUser)
			              VALUES ( @T_Day, @Is_Holiday, @Holiday_Date, @Location_Name, @Address, @MobileNo, @ImagePath, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Location_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and Location_Id<>@Location_Id)
		BEGIN	Update tbl_Location_Mst set  T_Day=@T_Day, Is_Holiday=@Is_Holiday, Holiday_Date=@Holiday_Date, Location_Name=@Location_Name, Address=@Address, MobileNo=@MobileNo, ImagePath=@ImagePath, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Location_Id=@Location_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Location_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Location_Id=@Location_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Membership_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Membership_Mst]
	@Member_Id	bigint = null,
	@ImagePath	nvarchar(MAX)	= null,
	@TTime_Id	bigint	= null,
	@Trainer_Id	bigint	= null,
	@Full_Name nvarchar(100)	= null,
	@Student_Name	nvarchar(100)	= null,
	@Address	nvarchar(MAX)	= null,
	@Zip_Code	numeric(18, 0)	= null,
	@Country	nvarchar(100)	= null,
	@MobileNo	numeric(18, 0)	= null,
	@Email_Id	nvarchar(150)	= null,
	@IBAN	nvarchar(150)	= null,
	@M_Option_Id	bigint	= null,
	@Price numeric(18, 2)	= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag ,M_Option_Id from  tbl_Membership_Mst Where Flag = 'A'
		
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag,M_Option_Id  from tbl_Membership_Mst Where Flag = 'D'
		
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag,M_Option_Id  from tbl_Membership_Mst 

		Select tm.Member_Id, tm.ImagePath, tm.TTime_Id, tm.Trainer_Id, tm.Student_Name, tm.Address, tm.Zip_Code, tm.Country, tm.MobileNo, tm.Email_Id, tm.IBAN, tm.Flag , tm.M_Option_Id, 
		((case when tttm.Is_Holiday = 'A' then CONVERT(nvarchar(50) ,tttm.Holiday_Date ,106) else tttm.T_Day end) + ' ( '+ tttm.Course_Form + ' - ' + tttm.Course_To +' ) at ' + tl.Location_Name )  as Locations,
		tt.Full_Name as Trainer_Name,
		case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select Package_Name from tbl_Membership_Options_Charges where M_Option_Id = tm.M_Option_Id )
				else 
					''
				end as Package_Name

		from tbl_Membership_Mst as tm

		inner join tbl_Trainer_mst as tt on tt.Trainer_Id = tm.Trainer_Id
		inner join tbl_Training_Timings as tttm on tttm.TTime_Id = tm.TTime_Id
		inner join tbl_Location_Mst as tl on tl.Location_Id = tttm.Location_Id
		
	END

	IF @Mode='SelectById'
	BEGIN
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag, M_Option_Id from tbl_Membership_Mst Where Member_Id = @Member_Id
	
		Select tm.Member_Id, tm.ImagePath, tm.TTime_Id, tm.Trainer_Id, tm.Student_Name, tm.Address, tm.Zip_Code, tm.Country, tm.MobileNo, tm.Email_Id, tm.IBAN, tm.Flag , tm.M_Option_Id, 
		
		case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select Is_Kide_Adult from tbl_Membership_Options_Charges where M_Option_Id = tm.M_Option_Id )
				else 
					''
				end as Is_Kide_Adult

		from tbl_Membership_Mst as tm Where Member_Id = @Member_Id

		Select * from tbl_Membership_Product_Mst Where M_Option_Id = (Select M_Option_Id from tbl_Membership_Mst Where  Member_Id = @Member_Id)
	
		
	END

	IF @Mode='SelectByIdforTrainer'
	BEGIN
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag,
			case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select top 1 BOD from tbl_Check_In_Mst where Probetraining_Id = tm.Member_Id order by CechkId desc)
				else 
					''
				end as BOD,

				case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select top 1 Belt from tbl_Check_In_Mst where Probetraining_Id = tm.Member_Id order by CechkId desc)
				else 
					''
				end as Belt,
				case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select top 1 NBTDate from tbl_Check_In_Mst where Probetraining_Id = tm.Member_Id order by CechkId desc)
				else 
					''
				end as NBTDate,
				case when Exists(Select Member_Id from tbl_Membership_Mst where Member_Id <> 0 and Member_Id = tm.Member_Id)
				then 
					(Select top 1 NBTTime from tbl_Check_In_Mst where Probetraining_Id = tm.Member_Id order by CechkId desc)
				else 
					''
				end as NBTTime

		from tbl_Membership_Mst as tm Where Member_Id = @Member_Id
	END

	IF @Mode='SelectByTrainerId'
	BEGIN
		Select Member_Id, ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag from tbl_Membership_Mst Where Trainer_Id = @Trainer_Id
	END


	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Membership_Mst where MobileNo = @MobileNo and  Email_Id= @Email_Id)
		BEGIN
		INSERT INTO tbl_Membership_Mst ( ImagePath, TTime_Id, Trainer_Id, Student_Name, Address, Zip_Code, Country, MobileNo, Email_Id, IBAN, Flag, CreateDate, CreateUser)
			              VALUES ( @ImagePath, @TTime_Id, @Trainer_Id, @Student_Name, @Address, @Zip_Code, @Country, @MobileNo, @Email_Id, @IBAN, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
		set @Member_Id = @@IDENTITY

		INSERT INTO tbl_Banking_Booking_Mst (Member_Id, Full_Name, Price, IBAN, Flag, CreateDate, CreateUser)
			              VALUES (@Member_Id, @Full_Name, @Price, @IBAN, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
		
			SET @intOutput = 'I'+';'+ CAST(@Member_Id as nvarchar(50));;
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode= @@ERROR
		END
	END

	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Membership_Mst where MobileNo = @MobileNo and Email_Id= @Email_Id and Member_Id<>@Member_Id)
		BEGIN	
		
		Update tbl_Membership_Mst set  ImagePath=@ImagePath, TTime_Id=@TTime_Id, Trainer_Id=@Trainer_Id, Student_Name=@Student_Name, Address=@Address, Zip_Code=@Zip_Code, Country=@Country, MobileNo=@MobileNo, Email_Id= @Email_Id, IBAN=@IBAN, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Member_Id=@Member_Id
			
		Update tbl_Banking_Booking_Mst set Member_Id=@Member_Id, Full_Name=@Full_Name, Price=@Price, IBAN=@IBAN, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Member_Id=@Member_Id

			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='UpdateOption'
	BEGIN
	
		Update tbl_Membership_Mst set  M_Option_Id=@M_Option_Id, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Member_Id=@Member_Id
		
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Membership_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Member_Id=@Member_Id
		
		Update tbl_Banking_Booking_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Member_Id=@Member_Id
		
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Membership_Options_Charges]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Membership_Options_Charges]
	@M_Option_Id	bigint	 = null,
	@Is_Kide_Adult	nchar(1)	 = null,
	@Starter_Price	numeric(18, 2)	 = null,
	@Package_Name	nvarchar(50)	 = null,
	@Package_Time	nvarchar(50)	 = null,
	@Times_Per_Week	nvarchar(50)	 = null,
	@Monthly_Investment	numeric(18, 2)	 = null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select M_Option_Id, Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag from tbl_Membership_Options_Charges 
		Where Flag = 'A'
		
		Select M_Option_Id, Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag from tbl_Membership_Options_Charges 
		Where Flag = 'D'
		
		Select M_Option_Id, Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag from tbl_Membership_Options_Charges 
	END

	IF @Mode='SelectById'
	BEGIN
		Select M_Option_Id, Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag from tbl_Membership_Options_Charges Where M_Option_Id = @M_Option_Id
	END

	IF @Mode='SelectByIsKideAdult'
	BEGIN
		Select M_Option_Id, Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag from tbl_Membership_Options_Charges Where Is_Kide_Adult = @Is_Kide_Adult
	END

	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Membership_Options_Charges where Package_Name= @Package_Name)
		BEGIN
		INSERT INTO tbl_Membership_Options_Charges (Is_Kide_Adult, Starter_Price, Package_Name, Package_Time, Times_Per_Week, Monthly_Investment, Flag, CreateDate, CreateUser)
			              VALUES ( @Is_Kide_Adult, @Starter_Price, @Package_Name, @Package_Time, @Times_Per_Week, @Monthly_Investment, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
		set @M_Option_Id = @@IDENTITY
			
			SET @intOutput = 'I'+';'+ CAST(@M_Option_Id as nvarchar(50));
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END


	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Membership_Options_Charges where Package_Name= @Package_Name and M_Option_Id<>@M_Option_Id)
		BEGIN	Update tbl_Membership_Options_Charges set Is_Kide_Adult=@Is_Kide_Adult, Starter_Price=@Starter_Price, Package_Name=@Package_Name, Package_Time=@Package_Time, Times_Per_Week=@Times_Per_Week, Monthly_Investment=@Monthly_Investment, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Option_Id=@M_Option_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
			BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Membership_Options_Charges set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Option_Id=@M_Option_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Membership_Product_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Membership_Product_Mst]
	@M_Product_Id	bigint = null,
	@M_Option_Id	bigint	= null,
	@Product_Name	nvarchar(100)= null,
	@ImagePath	nvarchar(MAX)= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select M_Product_Id, M_Option_Id, Product_Name, ImagePath, Flag from tbl_Membership_Product_Mst Where Flag = 'A'
		
		Select M_Product_Id, M_Option_Id, Product_Name,  ImagePath, Flag from tbl_Membership_Product_Mst Where Flag = 'D'
		
		Select M_Product_Id, M_Option_Id, Product_Name, ImagePath, Flag from tbl_Membership_Product_Mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select M_Product_Id, M_Option_Id, Product_Name, ImagePath, Flag from tbl_Membership_Product_Mst Where M_Product_Id = @M_Product_Id
	END

	IF @Mode='SelectByOptionId'
	BEGIN
		Select M_Product_Id, M_Option_Id, Product_Name, ImagePath, Flag from tbl_Membership_Product_Mst Where M_Option_Id = @M_Option_Id
		Select * from tbl_Membership_Options_Charges Where M_Option_Id = @M_Option_Id
	END


	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Membership_Product_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Membership_Product_Mst (M_Option_Id, Product_Name, ImagePath, Flag, CreateDate, CreateUser)
			              VALUES ( @M_Option_Id, @Product_Name, @ImagePath, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--END
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Membership_Product_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and M_Product_Id<>@M_Product_Id)
		--BEGIN	
		Update tbl_Membership_Product_Mst set M_Option_Id=@M_Option_Id, Product_Name=@Product_Name, ImagePath=@ImagePath, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Product_Id=@M_Product_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Membership_Product_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Product_Id=@M_Product_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END


GO
/****** Object:  StoredProcedure [dbo].[SP_Message_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Message_Mst] 
	@Message_Id	bigint	= null,
	@Message	nvarchar(MAX)	= null,
	@BVK	nvarchar(MAX)	= null,
	@Users	nvarchar(MAX)	= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Message_Id, Message, BVK, Users, Flag from tbl_Message_Mst Where Flag = 'A'
		
		Select Message_Id, Message, BVK, Users, Flag from tbl_Message_Mst Where Flag = 'D'
		
		Select Message_Id, Message, BVK, Users, Flag from tbl_Message_Mst
	END

	IF @Mode='SelectById'
	BEGIN
		Select Message_Id, Message, BVK, Users, Flag from tbl_Message_Mst Where Message_Id = @Message_Id
	END

	IF @Mode='Insert'
	BEGIN
		INSERT INTO tbl_Message_Mst ( Message, BVK, Users, Flag, CreateDate, CreateUser)
			              VALUES ( @Message, @BVK, @Users, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	END


	IF @Mode='Update'
	BEGIN
		Update tbl_Message_Mst set Message=@Message, BVK=@BVK, Users=@Users, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Message_Id=@Message_Id
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Message_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Message_Id=@Message_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Monthly_Bookings_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Monthly_Bookings_Mst]
	@M_Booking_Id	bigint	 = null,
	@Booking_Date	datetime	 = null,
	@Probetraining_Id	bigint	 = null,
	@Joining_Fee	numeric(18, 2)	 = null,
	@Monthly_Booking	numeric(18, 2)	 = null,
	@IBAN	nvarchar(50)	 = null,
	@Total	numeric(18, 2)	 = null,
	@IsPaid	nchar(1)	 = null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select tp.Student_Name as Full_Name, tmb.M_Booking_Id, tmb.Booking_Date, tmb.Probetraining_Id, tmb.Joining_Fee, tmb.Monthly_Booking, tmb.IBAN, tmb.Total, tmb.IsPaid, tmb.Flag from tbl_Monthly_Bookings_Mst as tmb
		inner join tbl_Membership_Mst as tp on tmb.Probetraining_Id = tp.Member_Id
		 Where tmb.Flag = 'A'
		
		Select tp.Student_Name as Full_Name, tmb.M_Booking_Id, tmb.Booking_Date, tmb.Probetraining_Id, tmb.Joining_Fee, tmb.Monthly_Booking, tmb.IBAN, tmb.Total, tmb.IsPaid, tmb.Flag from tbl_Monthly_Bookings_Mst as tmb
		inner join tbl_Membership_Mst as tp on tmb.Probetraining_Id = tp.Member_Id
		 Where tmb.Flag = 'D'
		
		Select tp.Student_Name as Full_Name, tmb.M_Booking_Id, tmb.Booking_Date, tmb.Probetraining_Id, tmb.Joining_Fee, tmb.Monthly_Booking, tmb.IBAN, tmb.Total, tmb.IsPaid, tmb.Flag from tbl_Monthly_Bookings_Mst as tmb
		inner join tbl_Membership_Mst as tp on tmb.Probetraining_Id = tp.Member_Id

		select sum(Total) as GrandTotal from tbl_Monthly_Bookings_Mst Where Flag = 'A'
	END

	IF @Mode='SelectById'
	BEGIN
		Select tp.Student_Name as Full_Name, tp.MobileNo,tp.Address,tp.Email_Id, tmb.M_Booking_Id, tmb.Booking_Date, tmb.Probetraining_Id, tmb.Joining_Fee, tmb.Monthly_Booking, tmb.IBAN, tmb.Total, tmb.IsPaid, tmb.Flag from tbl_Monthly_Bookings_Mst as tmb
		inner join tbl_Membership_Mst as tp on tmb.Probetraining_Id = tp.Member_Id
		Where tmb.M_Booking_Id = @M_Booking_Id

		Select * ,Company_Address_Line1 + ' '+ Company_Address_Line2 +' '+ Company_Street +' '+ Company_Nearby +' '+ Company_Area  +' '+ Company_City +' '+ Company_State  +' '+ Company_Country  as Address from tbl_Company_Detail_Mst Where Flag = 'A'

	END

	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Monthly_Bookings_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Monthly_Bookings_Mst ( Booking_Date, Probetraining_Id, Joining_Fee, Monthly_Booking, IBAN, Total, IsPaid, Flag, CreateDate, CreateUser)
			              VALUES ( @Booking_Date, @Probetraining_Id, @Joining_Fee, @Monthly_Booking, @IBAN, @Total, @IsPaid, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Monthly_Bookings_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and M_Booking_Id<>@M_Booking_Id)
		 Update tbl_Monthly_Bookings_Mst set Booking_Date=@Booking_Date, Probetraining_Id=@Probetraining_Id, Joining_Fee=@Joining_Fee, Monthly_Booking=@Monthly_Booking, IBAN=@IBAN, Total=@Total, IsPaid=@IsPaid, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Booking_Id=@M_Booking_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Monthly_Bookings_Mst set Flag=@Flag,UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where M_Booking_Id=@M_Booking_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
/****** Object:  StoredProcedure [dbo].[SP_Probetraining_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Probetraining_Mst]

	@Probetraining_Id	bigint	 = null,
	@Location_Id	bigint	 = null,
	@Trainer_Id	bigint	 = null,
	@Full_Name	nvarchar(100)	 = null,
	@Email_Id	nvarchar(150)	 = null,
	@MobileNo	numeric(18, 0)	 = null,
	@Age	bigint	 = null,
	@Heard	nvarchar(50)	 = null,
	@GoogleFacebook	nvarchar(MAX)	 = null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null

AS
BEGIN
	
	IF @Mode='SelectAll'
	BEGIN
		Select Probetraining_Id, Location_Id, Trainer_Id, Full_Name, Email_Id, MobileNo, Age, Heard, GoogleFacebook, Flag from tbl_Probetraining_Mst Where Flag = 'A'
		
		Select Probetraining_Id, Location_Id, Trainer_Id, Full_Name, Email_Id, MobileNo, Age, Heard, GoogleFacebook, Flag from tbl_Probetraining_Mst Where Flag = 'D'
		
		Select Probetraining_Id, Location_Id, Trainer_Id, Full_Name, Email_Id, MobileNo, Age, Heard, GoogleFacebook, Flag from tbl_Probetraining_Mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select Probetraining_Id, Location_Id, Trainer_Id, Full_Name, Email_Id, MobileNo, Age, Heard, GoogleFacebook, Flag from tbl_Probetraining_Mst Where Probetraining_Id = @Probetraining_Id
	END

	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Probetraining_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
		BEGIN
		INSERT INTO tbl_Probetraining_Mst ( Location_Id, Trainer_Id, Full_Name, Email_Id, MobileNo, Age, Heard, GoogleFacebook, Flag, CreateDate, CreateUser)
			              VALUES ( @Location_Id, @Trainer_Id, @Full_Name, @Email_Id, @MobileNo, @Age, @Heard, @GoogleFacebook, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END


	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Probetraining_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and Probetraining_Id<>@Probetraining_Id)
		BEGIN	Update tbl_Probetraining_Mst set  Trainer_Id=@Trainer_Id, Location_Id=@Location_Id, Full_Name=@Full_Name, Email_Id=@Email_Id, MobileNo=@MobileNo, Age=@Age, Heard=@Heard, GoogleFacebook=@GoogleFacebook, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Probetraining_Id=@Probetraining_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Probetraining_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Probetraining_Id=@Probetraining_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END

END

GO
/****** Object:  StoredProcedure [dbo].[SP_Product_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Product_Mst]
	@Product_Id	bigint= null,
	@Product_Name	nvarchar(100)= null,
	@Size	nvarchar(50)= null,
	@Price	numeric(18, 0)= null,
	@ImagePath	nvarchar(MAX)= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Product_Id, Product_Name, Size, Price, ImagePath, Flag from tbl_Product_Mst Where Flag = 'A'
		
		Select Product_Id, Product_Name, Size, Price, ImagePath, Flag from tbl_Product_Mst Where Flag = 'D'
		
		Select Product_Id, Product_Name, Size, Price, ImagePath, Flag from tbl_Product_Mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select Product_Id, Product_Name, Size, Price, ImagePath, Flag from tbl_Product_Mst Where Product_Id = @Product_Id
	END

	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Product_Mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Product_Mst (Product_Name, Size, Price, ImagePath, Flag, CreateDate, CreateUser)
			              VALUES ( @Product_Name, @Size, @Price, @ImagePath, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Product_Mst where Email_Id= @Email_Id and MobileNo = @MobileNo and Product_Id<>@Product_Id)
		BEGIN	Update tbl_Product_Mst set Product_Name=@Product_Name, Size=@Size, Price=@Price, ImagePath=@ImagePath, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Product_Id=@Product_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Product_Mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Product_Id=@Product_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Registration]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Registration]
	@Regi_Id	bigint = null,
	@Full_Name	nvarchar(100) = null,
	@Shcool_Name	nvarchar(100) = null,
	@Address	nvarchar(MAX) = null,
	@Zip_Code	numeric(18, 0) = null,
	@Country	nvarchar(100) = null,
	@Email_Id	nvarchar(150) = null,
	@MobileNo	numeric(18, 0) = null,
	@Flag nchar(1) = null,

	@Password nvarchar(50) = null,

	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Regi_Id, Full_Name, Shcool_Name, Address, Zip_Code, Country, Email_Id, MobileNo, Flag from  tbl_Registration Where Flag = 'A'
		
		Select Regi_Id, Full_Name, Shcool_Name, Address, Zip_Code, Country, Email_Id, MobileNo, Flag from tbl_Registration Where Flag = 'D'
		
		Select Regi_Id, Full_Name, Shcool_Name, Address, Zip_Code, Country, Email_Id, MobileNo, Flag from tbl_Registration 
	END

	IF @Mode='SelectById'
	BEGIN
		Select Regi_Id, Full_Name, Shcool_Name, Address, Zip_Code, Country, Email_Id, MobileNo, Flag from tbl_Registration Where Regi_Id = @Regi_Id
	END

	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Registration where Email_Id= @Email_Id or MobileNo = @MobileNo)
		BEGIN
		INSERT INTO tbl_Registration (Full_Name, Shcool_Name, Address, Zip_Code, Country, Email_Id, MobileNo, Flag, CreateDate, CreateUser)
			              VALUES ( @Full_Name, @Shcool_Name, @Address, @Zip_Code, @Country, @Email_Id, @MobileNo, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
		set @Regi_Id = @@IDENTITY

		--INSERT INTO tbl_Admin_Login (Regi_Id, Email_Id, Password, Flag, CreateDate, CreateUser)
		--	              VALUES (@Regi_Id, @Email_Id, @Password, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
		
			SET @intOutput = 'I'+';'+ CAST(@Regi_Id as nvarchar(50));
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END


	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Registration where Email_Id= @Email_Id and MobileNo = @MobileNo and Regi_Id<>@Regi_Id)
		BEGIN	Update tbl_Registration set Full_Name=@Full_Name, Shcool_Name=@Shcool_Name, Address=@Address, Zip_Code=@Zip_Code, Country=@Country, Email_Id=@Email_Id, MobileNo=@MobileNo, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Regi_Id=@Regi_Id
		
		--Update tbl_Admin_Login set Email_Id=@Email_Id, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Regi_Id=@Regi_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Registration set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Regi_Id=@Regi_Id
		
		--Update tbl_Admin_Login set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Regi_Id=@Regi_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Sms_Api_Mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[SP_Sms_Api_Mst]
	@ID	bigint	= NULL ,
	@ApiURL	nvarchar(MAX)	= NULL ,
	@UserName	nvarchar(100)	= NULL ,
	@Password	nvarchar(100)	= NULL ,
	@SenderID	nvarchar(100)	= NULL ,
	@Priorty	nvarchar(100)	= NULL ,
	@Type	nvarchar(100)	= NULL ,
	@ApiFlag	nvarchar(50)	= NULL ,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode nvarchar(50) = null
AS
BEGIN
	if @Mode = 'SelectAll'
	Begin
		select * from tbl_Sms_Api_Mst 

		select *,(case when ApiFlag='P' then 'Promotional' else case when ApiFlag='T' then 'Transaction' end end) as ApiFlagName from tbl_Sms_Api_Mst 
	End

	if @Mode = 'SelectByID'
	Begin
		select * from tbl_Sms_Api_Mst where ID=@ID
	End

	if @Mode = 'GetAPI'
	Begin
		select * from tbl_Sms_Api_Mst where ApiFlag=@ApiFlag
	End

	if @Mode = 'Insert'
	Begin
		insert into tbl_Sms_Api_Mst(ApiURL, UserName, Password, SenderID, Priorty, Type, ApiFlag)
		values (@ApiURL, @UserName, @Password, @SenderID, @Priorty, @Type, @ApiFlag)
			SET @intOutput = 'I';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	End

	if @Mode = 'Update'
	Begin
		update tbl_Sms_Api_Mst set ApiURL=@ApiURL, UserName=@UserName, Password=@Password, SenderID=@SenderID, Priorty=@Priorty, Type=@Type, ApiFlag=@ApiFlag where ID=@ID
			SET @intOutput = 'U';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	End

	if @Mode = 'Delete'
	Begin
		delete from tbl_Sms_Api_Mst where ID=@ID
			SET @intOutput = 'D';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	End
END



















GO
/****** Object:  StoredProcedure [dbo].[SP_Trainer_mst]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Trainer_mst]
	@Trainer_Id	bigint	= null,
	@ImagePath	nvarchar(MAX)	= null,
	@Full_Name	nvarchar(100)	= null,
	@Email_Id	nvarchar(150)	= null,
	@See	nvarchar(50)	= null,
	@Do	nvarchar(50)	= null,
	@Admin_Id	bigint	= null,
	@MobileNo	numeric(18, 0)	= null,
	@Password	nvarchar(50)	= null,
	@About	nvarchar(MAX)	= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null

AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select Trainer_Id, ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About, Flag from tbl_Trainer_mst Where Flag = 'A'
		
		Select Trainer_Id, ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About, Flag from tbl_Trainer_mst Where Flag = 'D'
		
		Select Trainer_Id, ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About, Flag from tbl_Trainer_mst 
	END

	IF @Mode='SelectById'
	BEGIN
		Select Trainer_Id, ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About, Flag from tbl_Trainer_mst Where Trainer_Id = @Trainer_Id
	END

	IF @Mode='Insert'
	BEGIN
	if not exists(select * from tbl_Trainer_mst where Email_Id= @Email_Id or MobileNo = @MobileNo)
	BEGIN
		INSERT INTO tbl_Trainer_mst ( ImagePath, Full_Name, Email_Id, See, Do, Admin_Id, MobileNo, Password, About, Flag, CreateDate, CreateUser)
			              VALUES ( @ImagePath, @Full_Name, @Email_Id, @See, @Do, @Admin_Id, @MobileNo, @Password, @About, @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END


	IF @Mode='Update'
	BEGIN
	if not exists(select * from tbl_Trainer_mst where Email_Id= @Email_Id and MobileNo = @MobileNo and Trainer_Id<>@Trainer_Id)
		BEGIN	Update tbl_Trainer_mst set ImagePath=@ImagePath, Full_Name=@Full_Name, Email_Id=@Email_Id, See=@See, Do=@Do, Admin_Id=@Admin_Id, MobileNo=@MobileNo, Password=@Password, About=@About, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Trainer_Id=@Trainer_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='UpdateProfile'
	BEGIN
	if not exists(select * from tbl_Trainer_mst where Email_Id= @Email_Id and MobileNo = @MobileNo and Trainer_Id<>@Trainer_Id)
		BEGIN	Update tbl_Trainer_mst set ImagePath=@ImagePath, Full_Name=@Full_Name, Email_Id=@Email_Id, MobileNo=@MobileNo, About=@About, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Trainer_Id=@Trainer_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			End
		ELSE 
		BEGIN
			SET @intOutput = 'E';
			SET @NumRowsChanged =  @@ROWCOUNT
			SET @ErrorCode=@@ERROR
		END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Trainer_mst set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where Trainer_Id=@Trainer_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END

GO
/****** Object:  StoredProcedure [dbo].[SP_Training_Timings]    Script Date: 30/11/2017 20:31 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[SP_Training_Timings]
	@TTime_Id	bigint	= null,
	@Location_Id	bigint	= null,
	@T_Day	nvarchar(100)	= null,
	@Is_Holiday	nchar(1)	= null,
	@Holiday_Date	datetime	= null,
	@Course_Id	bigint	=null,
	@Course_Form	nvarchar(50)	= null,
	@Course_To	nvarchar(50)	= null,
	@AddOnce	nvarchar(50)	= null,
	@EveryWeek	nvarchar(50)	= null,
	@Trainer_Id	bigint	= null,
	@Flag nchar(1) = null,
	@CreateUser	bigint = null,
	@UpdateUser	bigint = null,
	@intOutput nvarchar(50)=null OUTPUT,
	@NumRowsChanged nvarchar(50)=null OUTPUT,
	@ErrorCode nvarchar(50)=null OUTPUT,
	@Mode varchar(50) =null
	
AS
BEGIN
	IF @Mode='SelectAll'
	BEGIN
		Select TTime_Id, Location_Id, T_Day, Is_Holiday, Holiday_Date, Course_Id, Course_Form, Course_To, AddOnce, EveryWeek, Trainer_Id, Flag from tbl_Training_Timings Where Flag = 'A'
		 
		Select TTime_Id, Location_Id, T_Day, Is_Holiday, Holiday_Date, Course_Id, Course_Form, Course_To, AddOnce, EveryWeek, Trainer_Id,  Flag from tbl_Training_Timings Where Flag = 'D'
		
		Select TTime_Id, Location_Id, T_Day, Is_Holiday, Holiday_Date, Course_Id, Course_Form, Course_To, AddOnce, EveryWeek, Trainer_Id,  Flag from tbl_Training_Timings 
	END

	IF @Mode='SelectData'
	BEGIN
		Select Location_Id, Location_Name from tbl_Location_Mst Where Flag = 'A'
		 
		Select Trainer_Id, Full_Name from tbl_Trainer_mst Where Flag = 'A'

		Select Course_Id, Course_Name from tbl_Course_Mst Where Flag = 'A'

		Select tt.TTime_Id, ((case when tt.Is_Holiday = 'A' then CONVERT(nvarchar(50) ,tt.Holiday_Date ,106) else tt.T_Day end) + ' ( '+ tt.Course_Form + ' - ' + tt.Course_To +' ) at ' + tl.Location_Name )  as Locations from tbl_Training_Timings as tt
		inner join tbl_Location_Mst as tl on tl.Location_Id = tt.Location_Id
		 Where tt.Flag = 'A'
		
	END

	IF @Mode='SelectDatafroTainer'
	BEGIN
		 
		Select Trainer_Id, Full_Name from tbl_Trainer_mst Where Flag = 'A' and Trainer_Id=@Trainer_Id

		Select tt.*, tt.TTime_Id, ((case when tt.Is_Holiday = 'A' then CONVERT(nvarchar(50) ,tt.Holiday_Date ,106) else tt.T_Day end) + ' ( '+ tt.Course_Form + ' - ' + tt.Course_To +' ) at ' + tl.Location_Name )  as Locations from tbl_Training_Timings as tt
		inner join tbl_Location_Mst as tl on tl.Location_Id = tt.Location_Id
		 Where tt.Flag = 'A' and Trainer_Id=@Trainer_Id 
		
	END


	IF @Mode='SelectById'
	BEGIN
		Select TTime_Id, Location_Id, T_Day, Is_Holiday, Holiday_Date, Course_Id, Course_Form, Course_To, AddOnce, EveryWeek, Trainer_Id, Flag from tbl_Training_Timings Where TTime_Id = @TTime_Id
	END

	IF @Mode='Insert'
	BEGIN
	--if not exists(select * from tbl_Training_Timings where Email_Id= @Email_Id or MobileNo = @MobileNo)
	--	BEGIN
		INSERT INTO tbl_Training_Timings (Location_Id, T_Day, Is_Holiday, Holiday_Date, Course_Id, Course_Form, Course_To, AddOnce, EveryWeek, Trainer_Id,  Flag, CreateDate, CreateUser)
					VALUES (@Location_Id,  @T_Day, @Is_Holiday, @Holiday_Date, @Course_Id, @Course_Form, @Course_To, @AddOnce, @EveryWeek, @Trainer_Id,  @Flag, CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), @CreateUser)
	
			SET @intOutput = 'I';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			END
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END


	IF @Mode='Update'
	BEGIN
	--if not exists(select * from tbl_Training_Timings where Email_Id= @Email_Id and MobileNo = @MobileNo and TTime_Id<>@TTime_Id)
		BEGIN	Update tbl_Training_Timings set Location_Id=@Location_Id, T_Day=@T_Day, Is_Holiday=@Is_Holiday, Holiday_Date=@Holiday_Date, Course_Id=@Course_Id, Course_Form=@Course_Form, Course_To=@Course_To, AddOnce=@AddOnce, EveryWeek=@EveryWeek, Trainer_Id=@Trainer_Id, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where TTime_Id=@TTime_Id
			
			SET @intOutput = 'U';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode = @@ERROR
			--End
		--ELSE 
		--BEGIN
		--	SET @intOutput = 'E';
		--	SET @NumRowsChanged =  @@ROWCOUNT
		--	SET @ErrorCode=@@ERROR
		--END
	END

	IF @Mode='Delete'
	BEGIN
		Update tbl_Training_Timings set Flag=@Flag, UpdateDate=CONVERT(Datetime,SWITCHOFFSET(SYSDATETIMEOFFSET(), '+05:30')), UpdateUser=@UpdateUser Where TTime_Id=@TTime_Id
			
			SET @intOutput = 'D';
			SET @NumRowsChanged = @@ROWCOUNT
			SET @ErrorCode=@@ERROR
	END
END
GO
